#!/usr/bin/env perl

# This script converts EOL characters to unix format.
# It also fixes fasta/qual files where there is an extra whitespace at the beginning of the line.
use strict;
use warnings;

$| = 1;

my $in;
if ( @ARGV > 1 )
{
	die "\nUSAGE: $0 mac_formatted_file > unix_formatted_file\n";
} elsif (@ARGV)
{
    open($in, '<', $ARGV[0]) or die($!);
} else
{
    $in = *STDIN;
}

my $isFasta;
while ( my $line = <$in> )
{
	$line =~ s/\r\n?/\n/g;
    if ( !defined($isFasta) )
    {
        if ($line =~ /^>/) { $isFasta = 1 } else { $isFasta = 0 }
    }
    if ( $isFasta )
    {
        if ($line =~ /^>/) { print $line }
        elsif ($line =~ /^\n$/) { 1 } # remove blank lines
        elsif ($line =~ /^\s+(.+)$/) { print "$1\n" } # remove extra whitespace
        else { print $line }
    } else
    {
        print $line;
    }
}
close $in;
