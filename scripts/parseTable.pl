#!/usr/bin/env perl

use strict;
use warnings;

$| = 1;

my ( $inFile, $level, $readOrOtu, $countFileName, $fractionsFileName ) = @ARGV;

my $USAGE = "
USAGE:
$0 inFile level [r/c] countFileName fractionsFileName

r or c denote if the output should be for OTUs or reads abundance
";

unless ( $inFile
	&& $level
	&& $readOrOtu
	&& $countFileName
	&& $fractionsFileName )
{
	die $USAGE;
}
unless ( $readOrOtu eq "r" || $readOrOtu eq "c" ) { die $USAGE; }

open( IN, $inFile ) || die "ERROR: can't open $inFile\n\a";

# learn which line starts the taxonomy
my $taxField;
my @samples;
my %samplesHash;
{
	my $firstLine = <IN>;

	my @fields     = split( "\t", $firstLine );
	my $clusters   = shift @fields;               # first column has clusters
	my $i          = 0;
	my $data_field = 1;
	while ( my $field = shift @fields )
	{
		$i++;
		if ( $field =~ "Taxonomy" )
		{
			$taxField = $i;
			last;
		}
		elsif ( $field =~ "% identity" )
		{
			$data_field = 0;
			next;
		}
		elsif ($data_field)
		{
			$samplesHash{$i} = $field;
			push @samples, $field;
		}
	}
}

unless ($taxField)
{
	die "ERROR: Can't find taxonomic string field\n\a";
}

my %otuCountHash;
my %readsCountHash;
my %totalHash;
my %totalSampleHash;
while ( my $line = <IN> )
{
	chomp $line;
	if ( $line =~ /Potential chimeras:/ ) { last; }

	my @fields = split( "\t", $line );
	unless (@fields) { last; }
	my @tax_fields = @fields[ $taxField .. $#fields ];

	my $i = 0;
	my $taxon;
	unless ( $fields[$taxField] ) { $taxon = "Unknown"; }

	#    elsif($fields[$taxField] =~ /Eukaryota/){next;}
	#    else{print $fields[$taxField]; <STDIN>;}
	elsif ( $fields[ $taxField + $level - 1 ] )
	{
		$taxon = $fields[ $taxField + $level - 1 ];
	}
	else { $taxon = "Unknown"; }

	unless ($taxon) { die "ERROR: don't know the taxon\n"; }    # it's an euk

	#    my %localFieldsHash;
	my $fieldCount = 0;
	foreach my $field (@fields)
	{
		$fieldCount++;
		if ( $field eq "" || $field =~ /(\D+)/ ) { next; }

		if ( exists $samplesHash{ $fieldCount - 1 } )
		{
			my $sample = $samplesHash{ $fieldCount - 1 };
			$otuCountHash{$taxon}{$sample}++;
			$readsCountHash{$taxon}{$sample} += $field;
			if    ( $readOrOtu eq "c" ) { $totalHash{$taxon} += $field; }
			elsif ( $readOrOtu eq "r" ) { $totalHash{$taxon} += $field; }
			$totalSampleHash{$sample} += $field;

			#           $localFieldsHash{$fieldCount}=$field;
		}
	}

}
close IN;

my @taxa = keys %otuCountHash;
@taxa = sort { $totalHash{$b} <=> $totalHash{$a} } @taxa;
open( OUT, ">$countFileName" )
  || die "ERROR: can't open $countFileName for writing\n\a";
open( OUT_P, ">$fractionsFileName" )
  || die "ERROR: can't open $fractionsFileName for writing\n\a";

foreach my $sample (@samples)
{
	print OUT "\t",   $sample;
	print OUT_P "\t", $sample;
}
print OUT "\n";
print OUT_P "\n";

foreach my $taxon (@taxa)
{
	print OUT $taxon;
	print OUT_P $taxon;
	foreach my $sample (@samples)
	{
		print OUT "\t";
		print OUT_P "\t";
		if ( exists $otuCountHash{$taxon}{$sample} )
		{
			if ( $readOrOtu eq "c" )
			{
				print OUT $otuCountHash{$taxon}{$sample};
				print OUT_P "%.2f",
				  100 *
				  $otuCountHash{$taxon}{$sample} /
				  $totalSampleHash{$sample};
			}
			elsif ( $readOrOtu eq "r" )
			{
				print OUT $readsCountHash{$taxon}{$sample};
				printf OUT_P "%.2f",
				  100 *
				  $readsCountHash{$taxon}{$sample} /
				  $totalSampleHash{$sample};
			}
		}
	}
	print OUT_P "\n";
	print OUT "\n";

	#       print $taxon, "\t", $otuCountHash{$taxon}, "\n";
}

close OUT;
close OUT_P;
