#!/usr/bin/env perl

use strict;
use warnings;

$| = 1;

my ( $inFile, $blastFile ) = @ARGV;

unless ( $inFile && $blastFile )
{
	die "
USAGE:
$0 databaseFastaFile blastFile
";
}

open( IN, $inFile ) || die "ERROR: can't open $inFile\n\a";
my %namesHash;
while (my $line = <IN>)
{
	chomp $line;
	if ( $line =~ /^>([^ ]+) (.*)/ )
	{
		my $short_name = $1;

		$line =~ s/^>//;
		$line =~ s/ /; /g;
		$line =~ s/;/; /g;
		$line =~ s/  / /g;
		$namesHash{$short_name}=$line;
	}
}
close IN;

open( IN, $blastFile ) || die "ERROR: can't open $blastFile\n\a";
while (my $line = <IN>)
{
	chomp $line;
	if ( $line =~ /^(\S+)\s+(\S+)\s+(.+)$/ )
	{
		my $cluster = $1;
		my $seq = $2;
		my $data = $3;
		# Remove ";" in sequence name
		$seq =~ s/;//;
		if ( exists $namesHash{$seq} )
		{
			print $cluster, "\t", $namesHash{$seq}, "\t", $data, "\n";
		}
		else { die "$line\nNo database sequence for \"$seq\", aborting\a\n"; }
	}
	else
	{
		die "$line\nError reading input file, aborting\a\n";
	}
}
close IN;
