#!/usr/bin/env perl

use strict;
use warnings;

$| = 1;

my ( $phyloBlastFile, $repsFile ) = @ARGV;

unless ( $phyloBlastFile && $repsFile )
{
	die "
USAGE:
$0 blastFile fastaFile
";
}

my %blastHits;
open( BLAST, $phyloBlastFile ) || die "ERROR: can't open $phyloBlastFile\n";
while (my $line = <BLAST>)
{
	if ( $line =~ /^(\S+)\s/ ) { $blastHits{$1} = 1; }
}
close BLAST;

open( SEQS, $repsFile ) || die "ERROR: can't open $repsFile\n";
my $printit = 0;
while (my $line = <SEQS>)
{
	if ( $line =~ /^>(\S+)/ )
	{
		unless ( exists $blastHits{$1} ) { $printit = 1; }
		else                             { $printit = 0; }
	}
	if ($printit) { print $line; }
}
close SEQS;
