#!/usr/bin/env perl

use strict;
use warnings;

$| = 1;

my ( $clustersFile, $fastaFile ) = @ARGV;

unless ( $clustersFile && $fastaFile )
{
	die "
USAGE:
$0 clustersFile fastaFile
";
}

my %clusterSizes;
my %cluster2read;
open( IN, $clustersFile ) || die "ERROR: can't open $clustersFile\n\a";
my $cluster_number;
while (my $line = <IN>)
{
	chomp $line;
	if ( $line =~ /(\S+)/ )
	{
		$cluster_number++;
		my (@reads) = split /\s+/, $line;
		my $minCluster = 99999999999999999;
		my $cluster_;
		foreach my $read (@reads)
		{
			my ($number) = $read =~ /(\d+)/;
			if ( $minCluster > $number )
			{
				$minCluster = $number;
				$cluster_   = $read;
			}
			$clusterSizes{$cluster_number}++;
		}
		unless ($cluster_) { die "Error: can't find it for $cluster_number\n"; }
		$cluster2read{$cluster_number} = $cluster_;

	}
}
close IN;

#revert the hash inside out
my %reads2get;
my @clusters = keys %cluster2read;
my %reads2cluster;
foreach my $cluster (@clusters)
{
	my $read = $cluster2read{$cluster};
	$reads2get{$read}     = $clusterSizes{$cluster};
	$reads2cluster{$read} = $cluster;
}

open( IN, $fastaFile ) || die "ERROR: can't open $fastaFile\n";
my $seq;
my $seqName;
while (my $line = <IN>)
{
	chomp $line;

	if ( $line =~ /^>(\S+)/ )
	{
		if ($seqName)
		{
			if ( exists $reads2get{$seqName} )
			{
				my ($dataset) = $seqName =~ /(\S+)_/;
				my $clusterSize = $reads2get{$seqName};
				print ">", $seqName, "\n", $seq, "\n";

			}
		}
		$seqName = $1;
		$seq     = '';
	}
	else
	{
		$seq .= $line;
	}

}

if ( exists $reads2get{$seqName} )
{
	my ($dataset) = $seqName =~ /(\S+)_/;
	my $clusterSize = $reads2get{$seqName};
	print ">", $seqName, "_", $clusterSize, "\n", $seq, "\n";

}
close IN;
