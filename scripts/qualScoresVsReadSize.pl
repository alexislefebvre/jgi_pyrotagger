#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

$| = 1;

my $threshQual;
my $outFile;
GetOptions(
    'q=s' => \$threshQual,
    'o=s' => \$outFile
);
my @qualFiles = @ARGV;

# PROCESS QUAL FILES
my %goodSeqsHash;
foreach my $qualFile (@qualFiles)
{
	&readQualScores($qualFile, $threshQual, \%goodSeqsHash);
}

# OUTPUT
exit unless $outFile;
open(my $out, '>', $outFile) or die($!);
foreach my $qualFile (@qualFiles)
{
	my @path = split( '/', $qualFile );
	my ($number) = $path[$#path] =~ /(\d+)/;
	print $out "\t", $number;
}

print $out "\n";
my @seqLengths = keys %goodSeqsHash;
@seqLengths = sort { $a <=> $b } @seqLengths;
foreach my $length (@seqLengths)
{
	print $out $length;
	foreach my $qualFile (@qualFiles)
	{
		if ( exists $goodSeqsHash{$length}{$qualFile} )
		{
			print $out "\t", $goodSeqsHash{$length}{$qualFile};
		}
		else { print $out "\t"; }
	}
	print $out "\n";
}
exit;

sub readQualScores
{
	my ($qualFile, $threshQual, $goodSeqsHash) = @_;
	my $startTime = time;
    my $in;

    if ( $qualFile ne '-' and -e "$qualFile.good" )
    {
        # SIMPLY LOAD PREPROCESSED DATA
        open($in, '<', "$qualFile.good") or die($!);
        while (<$in>)
        {
            chomp;
            my ($position, $count) = split(/\t/);
            $goodSeqsHash->{$position}{$qualFile} = $count;
        }
        close($in);
        return;
    } elsif ( $qualFile eq '-' )
    {
        $in = *STDIN;
    } else
    {
        open( $in, '<', $qualFile ) || die "ERROR: can't open $qualFile: $!\n\a";
    }
	my $seqName;
	my $badBases            = 0;
	my $qualityThreshold    = 27;
	my $startCountingLength = 149;

	#    my $startCountingLength = 209;
	my $position = 0;
	while ( my $line = <$in> )
	{
		chomp $line;
		if ( $line =~ /^>(\S+)/ )
		{
			$seqName  = $1;
			$badBases = 0;
			$position = 0;
		}
		elsif ($line)
		{
			my @lineArr = split /\s+/, $line;
            next unless @lineArr;
            shift @lineArr if $lineArr[0] eq '';
			foreach my $qual_score (@lineArr)
			{
				$position++;
                unless ( $qual_score =~ /^\d+$/ )
                {
                    warn("Invalid qual score: $qual_score on line:\n$line\n");
                    last;
                }
				if ( $qual_score < $qualityThreshold )
				{
					$badBases++;
				}
				my $qualitySequenceThreshold =
				  int( $position * $threshQual / 100 );
				if (   $badBases <= $qualitySequenceThreshold
					&& $position > $startCountingLength )
				{

					#		    if($position < 300){
					$goodSeqsHash->{$position}{$qualFile}++;
				}
			}
		}
	}
	close($in) if $qualFile;
	my $endTime     = time;
	my $runningTime = $endTime - $startTime;
}
