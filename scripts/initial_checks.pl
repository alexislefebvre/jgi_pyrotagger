#!/usr/bin/env perl

use strict;
use warnings;
use MIME::Lite;
use FindBin qw($Bin);
use File::Copy;
use Config::Simple;

# CONFIG
my %Config;
Config::Simple->import_from( "$Bin/../pyrotagger.ini", \%Config );

my ( $qualThresh, $files_index_file, $dir, $id, $email, $url ) = @ARGV;

unless ( $qualThresh && $files_index_file && $dir )
{
	die "
USAGE:
$0 qualThresh files_index_file dir
";
}

unless ( $dir =~ /\/$/ )
{
	$dir .= "/";
}

my $primers2fasta = $dir . "primers2fasta.txt";
open( INDEX, "> $primers2fasta" )
  || &suicide("Can't open $primers2fasta for writing\n");

open( IN, $files_index_file )
  || &suicide("ERROR: can't open $files_index_file\n");
my @files_index_lines = <IN>;
my $i                 = 0;
while ( my $line = $files_index_lines[$i] )
{
	chomp $line;
	$i++;

	#	my ($primers, $fasta, $qual) = split("\t", $line);
	foreach my $file ( split( "\t", $line ) )
	{
		unless ( -e $file )
		{
			die "$file doesn't exist\n";
		}
	}
}

my @quals;
$i = 0;
while ( my $line = $files_index_lines[$i] )
{
	chomp $line;
	my ( $primers, $fasta, $qual ) = split( "\t", $line );
	$i++;
	my ($fastaFile, $fastaSeqs) = &runSavingChecks( $fasta, $i . ".fasta",      $dir );
	my ($qualFile, $qualSeqs)  = &runSavingChecks( $qual,  $i . ".fasta.qual", $dir );
	my ($primersFile) = runSavingChecks( $primers, $i . ".primers_wobble", $dir );

	if ( $fastaSeqs == $qualSeqs )
	{
		print STDERR "Looks good, number of sequences match\n<br>";
	}
	else
	{
		&suicide(
			"Error: number of sequences in fasta and qual file do not match (fasta=$fastaSeqs; qual=$qualSeqs)\n"
		);
	}

	my $com = "$Bin/translatePrimers.pl $primersFile > $dir/$i.primers";
	open( COM, "$com |" );
	close COM;
	$primersFile = "$dir/$i.primers";

	# verify that primers are ok, read their length
	print STDERR "Verifying primers file for $i...";
	$com = "$Bin/selectReadsMatchingBarcodes.pl $primersFile $fastaFile $qualFile";
	my $newFasta = $fastaFile . ".primers_only.fasta";
	my $newQual  = $newFasta . ".qual";

	#	print "<pre>";
	open( COM, "$com|" );
	print STDERR <COM>;
	close COM;

	#	print "</pre>";
	unless ( -s $newFasta && -s $newQual )
	{
		&suicide(
			"<font color=red>ERROR: after selecting primers, your dataset appears to be empty. Aborting.</font>\n<BR>"
		);
	}
	unlink $fastaFile;
	unlink $qualFile;
	print STDERR "ok\n";

	rename $newFasta, $fastaFile;
	rename $newQual,  $qualFile;

	print INDEX "$i.primers\t$i.fasta\n";

	push( @quals, $qualFile );
}

#   	my $errorsFile = $dir . "trim.errors";
#    my $stdoutFile = $dir . "trim.stdout";
#    my $successFile = $dir . "trimmingDone";

my $seqHashFile = $dir . "sizesHash.txt";
print "Preparing read size distribution chart (can take several minutes)...\n";
my $makeSizesDistributionPlotCom =
    "$Bin/qualScoresVsReadSize.pl -q $qualThresh -o $seqHashFile "
  . join( " ", @quals );
system $makeSizesDistributionPlotCom;
print STDERR "Done\n";
my $success_file = $dir . "checks_successful";
system "touch $success_file";

close IN;

# Script ended succesfully

my $checksUrl = $Config{cgi_url}.'/sendFile.pl?id='.$id.'&fileName=checks_errors';

# mail to the user if email is present
my $msg = MIME::Lite->new(
	From    => 'eskirton@lbl.gov',
	To      => $email,
	Subject => 'Initial checks for run ' . $id,
	Type    => 'multipart/mixed', );
$msg->attach(
	Type => 'TEXT/HTML',
	Data => "
Dear User,
<P>
Initial checks have finished for $id
</P>
<P>
<A HREF='$url'>$url</a>
</P>
<P>
<A HREF='$checksUrl'>$checksUrl</a>
</P>
<B>Please site: <A HREF='http://www.theopenjournal.org/toj_articles/1'>Kunin V., Hugenholtz, P. PyroTagger: A fast, accurate pipeline for analysis of rRNA amplicon pyrosequence data. The Open Journal, 
Article 1, Feb 2010.</A></B>
</P><P>
Sincerely yours, 
</P><P>
      The PyroTagger team.
", );

$msg->send('smtp', $Config{smtp_host}, Debug=>1 );

sub runSavingChecks
{
	my ( $file, $type, $dir ) = @_;
	print STDERR "checking $file $type\n";
    print "checking $type file\n<br>\n";

	my $final_filename = $dir . $type;
	my $filename       = $final_filename;
    my $tmpfile;
    my $count;
    my $cmd;
	if ( $file =~ /\.gz$/ )
	{
		$cmd = "gunzip -c $file";
	}
	elsif ( $file =~ /\.zip$/ )
	{
		$cmd = "/usr/bin/unzip -p $file";
	} elsif ( $file =~ /\.bz2$/ )
    {
		$cmd = "bzcat $file";
    } else
    {
        $tmpfile = "$final_filename.tmp";
        move($file, $tmpfile);
        $cmd = "cat $tmpfile";
    }
    $cmd .= " | $Bin/reformat.pl | tee $final_filename | grep -c '^>'";
    $count = `$cmd`;
    chomp $count;
    unlink($tmpfile) if $tmpfile;
    unless ( -s $final_filename )
    {
        &suicide("ERROR preprocessing $type, aborting.\n");
    }
	return ($final_filename, $count);
}

sub suicide
{
	my $message = shift @_;
	print STDERR $message;
	my $checks_failed_file = $dir . "checks_failed";
	system "touch $checks_failed_file";
	exit;
}
