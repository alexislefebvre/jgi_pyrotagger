#!/usr/bin/env perl

# EK 3/27/13: combined runLucyTrim.pl, lucyTrim.sh, and figaro_trim_seqs.pl into one script

use strict;
use warnings;
use IO::File;
use FindBin qw($Bin);
use lib "$Bin/../perl/share/perl/5.10.1";
use Config::Simple;

# CONFIG
my %Config;
Config::Simple->import_from( "$Bin/../pyrotagger.ini", \%Config );
die("bin_dir not defined\n") unless exists( $Config{bin_dir} );
die("bin_dir does not exist\n") unless -d $Config{bin_dir};

# INPUT
die("USAGE: $0 fastaFile qualFile stringency\n- stringency should be % of errorless bases, i.e. 99.8\n"
) unless @ARGV == 3;
my ( $fastaFile, $qualFile, $stringency ) = @ARGV;
die("Fasta file, $fastaFile, not found\n") unless $fastaFile and -f $fastaFile;
die("Qual file, $qualFile, not found\n")   unless $qualFile  and -f $qualFile;
die("Invalid stringency, $stringency (valid range is 50-100)\n")
  unless $stringency >= 50 and $stringency <= 100;

# LOG
my $logFile = $fastaFile . ".$stringency.log";

# RUN LUCY
my $err          = 1.0 - $stringency / 100;
my $outFastaFile = "$err.fa";
my $outQualFile  = "$err.fa.qual";
my $tmpFastaFile = "lucy.$err.fa";
my $tmpQualFile  = "lucy.$err.fa.qual";
my $cmd =
  "$Config{bin_dir}/lucy -error $err $err -bracket 20 $err -debug -xtra 6 -output $tmpFastaFile $tmpQualFile $fastaFile $qualFile &> $logFile";
system($cmd);
die($!) unless $? == 0;

# GET TRIMPOINTS
#$cmd = "grep '^>' $tmpFastaFile | sed 's/>//' | cut -d ' ' -f 1,5,6 > trim.points";
#system($cmd);
#die($!) unless $? == 0;

# TRIM
#my $cin  = new IO::File("<trim.points")   or die($!);
my $fin  = new IO::File("<$tmpFastaFile")    or die($!);
my $qin  = new IO::File("<$tmpQualFile")     or die($!);
my $fout = new IO::File(">$outFastaFile") or die($!);
my $qout = new IO::File(">$outQualFile")  or die($!);
my $seq = '';
my $len;
my $id;
my $start;
my $stop;
while ( my $line = <$fin> )
{
	chomp $line;
	if ( $line =~ /^>(\S+)\s+\d+\s+\d+\s+\d+\s+(\d+)\s+(\d+)/ )
	{
		if ( $seq )
		{
			my ($next_id, $next_start, $next_stop) = ($1,$2,$3);
			# PROCESS FASTA
			$len = length($seq);
			$seq = substr( $seq, $start - 1, $stop - $start + 1 );
			print $fout ">$id\n$seq\n";
			# PROCESS QUAL
			my $qualAR = get_next_qual($qin,$id,$len);
			print $qout ">$id\n", join(' ', @$qualAR[ $start - 1 .. $stop - 1]), "\n";
			# READY FOR NEXT
			($id, $start, $stop, $seq) = ($next_id, $next_start, $next_stop, '');	
		}
		else
		{
			($id, $start, $stop, $seq) = ($1, $2, $3, '');
		}
	}
	else
	{
		$seq .= $line;
	}
}
# PROCESS LAST FASTA
$len = length($seq);
$seq = substr( $seq, $start - 1, $stop - $start + 1 );
print $fout ">$id\n$seq\n";
# PROCESS LAST QUAL
my $qualAR = get_next_qual($qin,$id,$len);
print $qout ">$id\n", join(' ', @$qualAR[ $start - 1 .. $stop - 1]), "\n";

#unlink($tmpFastaFile,$tmpQualFile);

# STATS
$cmd = "$Config{bin_dir}/faSize $outFastaFile > $outFastaFile.stats";
system($cmd);
die($!) unless $? == 0;
exit;

## SUBROUTINES

sub get_next_qual
{
	my ($fh,$id,$len) = @_;
	my @qual = ();
	my $line = <$fh>;
	die("Invalid qual file\n") unless $line =~ /^>(\S+)/;
	die("Extra qual record in file: $1\n") unless $1 eq $id;
	while (	scalar(@qual) < $len )
	{
		$line = <$fh>;
		die("Truncated Qual file\n") unless defined($line);
		die("Missing qual values for $id\n") if $line =~ /^>/;
		chomp $line;
		push @qual, split( /\s+/, $line );
	}
	return \@qual;
}

# REFORMAT TO 60 CHARS PER LINE
sub outputFasta
{
	my ( $fh, $id, $seq ) = @_;
	die unless defined($fh) and length($id) and defined($seq);
	print $fh ">$id\n";
	while ( length($seq) > 60 )
	{
		print $fh substr( $seq, 0, 60 ), "\n";
		$seq = substr( $seq, 60 );
	}
	print $fh $seq, "\n";
}

# REFORMAT TO 60 VALUES PER LINE
sub outputQual
{
	my ( $fh, $id, $ar ) = @_;
	my @qual = @$ar;
	die unless defined($fh) and length($id) and defined($ar);
	print $fh ">$id\n";
	while ( scalar(@qual) > 60 )
	{
		print $fh join( ' ', @qual[ 0 .. 59 ] ), "\n";
		@qual = @qual[ 60 .. $#qual ];
	}
	print $fh join( ' ', @qual ), "\n";
}
