#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($Bin);
use lib "$Bin/../lib";
use mapClusters;

$| = 1;

my ( $finalSequenceFile, $superclustersFile, $clustersFile, $phyloMapFile,
	$primer_fasta_pairs_file, $chimerasFile, $prefix )
  = @ARGV;

unless ( $finalSequenceFile
	&& $superclustersFile
	&& $clustersFile
	&& $phyloMapFile
	&& $primer_fasta_pairs_file )
{
	die "
USAGE:
$0 final_sequenceFile superclustersFile clustersFile phyloMapFile primer_fasta_pairs_file [chimerasFile]
";
}

my ( $clustersOfClustersHashRef, $parentClustersHashRef, $subClusters2readsRef )
  = &loadDataStructures( $finalSequenceFile, $superclustersFile,
	$clustersFile );

my %fastaFilesHash;
my %datasetsHash;

my ( $datasetsHashRef, $datasetNamesHashRef ) =
  &readPrimersAndFasta_AssignDatasets( $primer_fasta_pairs_file,
	\%datasetsHash );
%datasetsHash = %{$datasetsHashRef};

print STDERR scalar keys %datasetsHash;
print STDERR " reads assigned in total\n";

my $primersCountHash;
my %chimerasHash;
if ($chimerasFile)
{
	open( IN, $chimerasFile ) || die "Error: can't read chimera's file\n";
	while ( my $line = <IN> )
	{
		chomp $line;
		$line =~ s/Potential chimeras: //;
		my @chimera_clusters = split( ", ", $line );
		foreach my $cluster (@chimera_clusters)
		{
			$chimerasHash{$cluster} = 1;
		}
	}
	close(IN);
}

#map clusters to names using PhyloMapFile
open( IN, $phyloMapFile ) || die "ERROR: can't open $phyloMapFile\n\a";
my %cluster2phylogtnyHash;
while (my $line = <IN>)
{
	chomp $line;
	$line =~ s/\r//g;
	if ( $line =~ /^Cluster(\d+)\t(.+)$/ )
	{
		$cluster2phylogtnyHash{$1} = $2;
	}
}
close IN;

# print out a file that will map reads to final clusters (superclusters)
my $clusters2seqsMapFile     = $prefix . ".clusters2reads.txt";
my $clusters2seqsChimeraFile = $prefix . ".clusters2reads.chimeras.txt";
my $unifracFile              = $prefix . ".unifrac.txt";
my $reads2datasetsFile       = $prefix . ".datasets2reads.txt";

#open (CHIMERA, ">$clusters2seqsChimeraFile") || die "ERROR: can't write to $clusters2seqsChimeraFile\n";
open( OUT, ">$clusters2seqsMapFile" )
  || die "ERROR: can't write to $clusters2seqsMapFile\n\a";
open( UNIFRAC, ">$unifracFile" ) || die "ERROR: can't write to $unifracFile\n";
open( DATASETS2READS, ">$reads2datasetsFile" )
  || die "ERROR: can't write to $reads2datasetsFile\n";

# count datasets in superclusters
my @superclusters = keys %{$clustersOfClustersHashRef};
my %superClustersTotalHash;

@superclusters = sort { $a <=> $b } @superclusters;

my %superClusters2primers;
my %primersCountHash;
foreach my $supercluster (@superclusters)
{
	my $print2;

#   if(exists $chimerasHash{"Cluster" . $supercluster})    {   $print2 = *CHIMERA;    }
#   else{$print2 = *OUT;}

	$print2 = *OUT;
	print $print2 "Cluster", $supercluster;
	my @subclusters = keys %{ ${$clustersOfClustersHashRef}{$supercluster} };

	foreach my $subcluster (@subclusters)
	{
		my @reads = keys %{ $$subClusters2readsRef{$subcluster} };
		foreach my $read (@reads)
		{
			my $primerName = $datasetsHash{$read};

			unless ($primerName)
			{
				print STDERR "WARNING: can't find info for read $read\n";
				next;
			}
			print $print2 "\t", $read;
			print DATASETS2READS $primerName, "\t", $read, "\n";
			$superClustersTotalHash{$supercluster}++;

			$superClusters2primers{$supercluster}{$primerName}++;
			$primersCountHash{$primerName}++;
		}
	}
	print $print2 "\n";
}

#close CHIMERA;
close OUT;

my $fractionTableFile = $prefix . ".cluster_classification_percent.xls";
open( OUT, ">$fractionTableFile" )
  || die "ERROR: can't write to $fractionTableFile\n\a";

my @primers = keys %{$datasetNamesHashRef};
@primers = sort @primers;
foreach my $set (@primers)
{
	print OUT "\t", $set;
	print "\t", $set;
}

#print "\tBest blast match";
print
  "\t% identity\tAlignment Length\tMismatches\tGaps\tQuery Start\tQuery End\tHit Start\tHit End\tE-value\tScore\tID\tFull Name\tTaxonomy\n";
print OUT
  "\t% identity\tAlignment Length\tMismatches\tGaps\tQuery Start\tQuery End\tHit Start\tHit End\tE-value\tScore\tID\tFull Name\tTaxonomy\n";

my $totalReads = 0;
my @chimeraClusters;
foreach my $cluster (@superclusters)
{
	if (   $chimerasHash{ "Cluster" . $cluster }
		&& $superClustersTotalHash{$cluster} < 3 )
	{
		push( @chimeraClusters, $cluster );
		next;
	}

	print OUT "Cluster", $cluster;
	print "Cluster", $cluster;
	&printCluster($cluster);
}

if (@chimeraClusters)
{
	print "\n\nPotential chimeras:\n";
	print OUT "\n\nPotential chimeras:\n";

	foreach my $cluster (@chimeraClusters)
	{
		if ( $chimerasHash{$cluster} ) { next; }

		print OUT "Cluster", $cluster;
		print "Cluster", $cluster;
		&printCluster($cluster);
	}
}

sub printCluster
{
	my $cluster = shift @_;

	foreach my $set (@primers)
	{
		print "\t";
		print OUT "\t";
		if ( exists $superClusters2primers{$cluster}{$set} )
		{
			printf OUT "%.2f",
			  100 *
			  $superClusters2primers{$cluster}{$set} /
			  $primersCountHash{$set};
			print $superClusters2primers{$cluster}{$set};
			print UNIFRAC "Cluster", $cluster, "\t", $set, "\t",
			  $superClusters2primers{$cluster}{$set}, "\n";
		}
	}
	if ( exists $cluster2phylogtnyHash{$cluster} )
	{
		my ( $phylogenyString, @blast_fields ) =
		  split( "\t", $cluster2phylogtnyHash{$cluster} );

		#       print "\n";
		foreach my $field (@blast_fields)
		{
			print "\t", $field;
			print OUT "\t", $field;
		}

		#       <STDIN>;
		my @phylogeny = split( "; ", $phylogenyString );
		my $species_name = pop @phylogeny;
		print "\t", $species_name;
		print OUT "\t", $species_name;
		foreach my $phylo (@phylogeny)
		{
			print "\t", $phylo;
			print OUT "\t", $phylo;
		}

		#       "<STDIN>";
		#       print "\t", $cluster2phylogtnyHash{$cluster};
	}
	print "\n";
	print OUT "\n";
}

close OUT;
close UNIFRAC;
close DATASETS2READS;
