#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($Bin);
use lib "$Bin/../lib";
use mapClusters;

$| = 1;

my ( $primersFile, $fastaFile, $qualFile, $progress ) = @ARGV;

unless ( $primersFile && $fastaFile )
{
	die "
USAGE:
$0 primersFile fastaFile [qualFile]

- Qual file is optional.
put a 4th argument if you want progress of the run to be reported.
";
}

unless ( -s $primersFile && -s $fastaFile && -s $qualFile )
{
	die "
ERROR: one or more input files is empty or doesn't exist.
";
}

my ($primersHashRef) = &readPrimers($primersFile);

my $primerLength;
{
	my @primers = keys %{$primersHashRef};
	foreach my $primer (@primers)
	{
		my $tmp_length = length $primer;
		if ($primerLength)
		{
			unless ( $tmp_length == $primerLength )
			{
				die "primers don't have the same length, aborting\n\a";
			}
		}
		else { $primerLength = $tmp_length; }
	}
}

open( IN, $fastaFile ) || die "ERROR: can't open $fastaFile\n";
my $fastaOut = $fastaFile . ".primers_only.fasta";
open( OUT, "> $fastaOut" ) || die "ERROR: can't write to $fastaOut\n";
my $seqName;
my $seq = '';
my $primer;

my %datasetsHash;
my %seqsHash;
my $count    = 0;
my $noPrimer = 0;
while ( my $line = <IN> )
{
	chomp $line;
	if ( $line =~ /^>(\S+)/ )
	{
		my $tmpSeqName = $1;
		my $seqLength  = length $seq;
		my $primerRec  = substr( $seq, 0, $primerLength );
		if ( exists $$primersHashRef{$primerRec} ) { $primer = $primerRec }
		elsif ($seq) { $noPrimer++; }
		if ( $seqName && $primer && $seqLength > 100 )
		{
			$datasetsHash{$primer}++;
			$seqsHash{$seqName} = $primer;

			#	    print STDERR $seqName, "\n";
			#	    print STDERR $line;
			#	    <STDIN>;
			print OUT ">", $seqName, "\n", $seq, "\n";
			$count++;
			if ( $progress && $count / 10000 == int( $count / 10000 ) )
			{
				print "\rProcessing reads: ", $count;
			}
		}
		$seqName = $tmpSeqName;
		$seq     = '';
		$primer  = '';
	}
	else
	{
		$seq .= uc $line;
	}
}

close IN;

if ( $seqName && $primer )
{
	$datasetsHash{$primer}++;
	$seqsHash{$seqName} = $primer;
	print OUT ">", $seqName, "\n", $seq, "\n";
	$count++;
}
close OUT;
print "\nSaved $count reads, $noPrimer sequences had no matches to primers\n";

if ($qualFile)
{
	if ($progress) { print "processing qual file.\n"; }
	open( IN, $qualFile ) || die "ERROR: can't open $qualFile\n";
	my $qualOut = $fastaOut . ".qual";
	open( OUT, "> $qualOut" ) || die "ERROR: can't write to $qualOut\n";
	my $seqName;
	my $seq;
	my $primer;
	my $qual_count   = 0;
	my $missed_count = 0;

	while ( my $line = <IN> )
	{
		chomp $line;
		if ( $line =~ /^>(\S+)/ )
		{
			my $tmpSeqName = $1;
			if ( $seqName && exists $seqsHash{$seqName} )
			{
				print OUT ">", $seqName, "\n", $seq, "\n";
				$qual_count++;
				if (   $progress
					&& $qual_count / 1000 == int( $qual_count / 1000 ) )
				{
					print "\rKept: ", $qual_count, "\t";
					print "Discarded: ", $missed_count;
				}
			}
			else
			{
				$missed_count++;
				if (   $progress
					&& $missed_count / 1000 == int( $missed_count / 1000 ) )
				{
					print "\rKept: ", $qual_count, "\t";
					print "Discarded: ", $missed_count;
				}
			}

			#	    $seqName = $line;
			$seqName = $tmpSeqName;
			$seq     = '';
			$primer  = '';
		}
		else
		{
			if (   $seq
				&& $seq =~ /\d+$/ )   # if the sequence ends with non-space char
			{
				$seq .= " ";
			}
			$seq .= $line;
		}
	}
	if ( $seqName && exists $seqsHash{$seqName} )
	{
		print OUT ">", $seqName, "\n", $seq, "\n";
		$qual_count++;
	}
	close IN;
	close OUT;
	print "\nQuality scores for $qual_count sequences kept\n";
}
