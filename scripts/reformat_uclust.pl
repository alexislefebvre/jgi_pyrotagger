#!/usr/bin/env perl

use strict;
use warnings;

$| = 1;

my ( $ucFile, $fastaFile, $repsFile, $readsPerClusterFile ) = @ARGV;

unless ( $ucFile && $fastaFile && $repsFile && $readsPerClusterFile )
{
	die "
USAGE:
$0 uclustFile fastaFile repsFile readsPerClusterFile
";
}

open( IN, $ucFile ) || die "ERROR: can't open $ucFile\n\a";
my %clustersHash;
my %clusterRepsHash;
my %clusterRepsCount;
my @clusters;
print STDERR "Reading uclust file\n";
while ( my $line = <IN> )
{
	chomp $line;
	if ( $line =~ /\#/ ) { next; }
	if ( $line =~
		/^H\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/ )
	{
		my $cluster = $1;
		my $seqName = $8;
		push @{ $clustersHash{$cluster} }, $seqName;
		$clusterRepsCount{$cluster}++;
	}
	elsif ( $line =~
		/^S\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/ )
	{
		my $cluster = $1;
		my $seqName = $8;
		push @{ $clustersHash{$cluster} }, $seqName;
		$clusterRepsHash{$seqName} = $cluster;
		$clusterRepsCount{$cluster}++;
		push @clusters, $cluster;
	}
}
close IN;

print STDERR "Reading fasta file\n";
my $seqName = "";
my $seq     = "";
open( IN,  $fastaFile )    || die "ERROR: can't open $fastaFile\n\a";
open( OUT, "> $repsFile" ) || die "ERROR: can't write to $repsFile\n\a";
while ( my $line = <IN> )
{
	chomp $line;
	if ( $line =~ /^>(\S+)/ )
	{
		if ( exists $clusterRepsHash{$seqName} )
		{
			print OUT ">$seqName\n$seq\n";
		}
		$seqName = $1;
		$seq     = "";
	}
	else { $seq .= $line; }
}
if ( exists $clusterRepsHash{$seqName} )
{
	print OUT ">$seqName\n$seq\n";
}
close IN;
close OUT;

print STDERR "Writing clusters2reads file\n";
open( OUT, "> $readsPerClusterFile" )
  || die "ERROR: can't write to $readsPerClusterFile";
foreach my $cluster (@clusters)
{

	#    print OUT $cluster;
	foreach my $seq ( @{ $clustersHash{$cluster} } )
	{
		print OUT "$seq\t";
	}
	print OUT "\n";
}
close OUT;
