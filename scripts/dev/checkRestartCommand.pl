#!/usr/bin/env perl

use strict;
use warnings;

#my $path = shift; #path to script should we need to relaunch it
#my ($path, $scriptName, $nice) = @ARGV; #name of script to look for

#unless($path && $scriptName)
#{
#    die "
#USAGE:
#$0 path scriptName nice
#
#path - path to script should we need to relaunch it
#scriptName - name of script to look for
#";
#}
my $com = "/home/mepweb/programs/clusterCron.sh /home/mepweb/tasks";
my $scriptName = "clusterCron.sh";

my $cmd = "ps aux | grep vkunin | grep " . $scriptName;
my $result = `$cmd`;
my @lines = split("\\n", $result);

foreach my $line (@lines) 
{
    if($line =~ /grep/){next;}    #ignore results for the grep command and this script
    if($line =~ $scriptName)    {	exit;    }
}

my $launchCMD = "nohup $com >& /dev/null 2>&1 &";
#my $tmpFile = "/tmp/$$";
#open(TMP, "> $tmpFile") || die "ERROR: can't write to $tmpFile\n";
#print TMP "#!/usr/bin/tcsh\n";
#print TMP $launchCMD, "\n";
#close TMP;
print STDERR "Lounching $launchCMD\n";
system($launchCMD);
#system("source $launchCMD");
#unlink $tmpFile;
