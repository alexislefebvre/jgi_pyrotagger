#!/usr/bin/env perl

use strict;
use warnings;

$|=1;

my ($inFile) = @ARGV;

unless ($inFile)
{
    die "
USAGE:
$0 $fastaFile $qualFile
";
}

print "Counting sequences in Fasta file $fastaFile    ";
my $grepcom = "/bin/grep -c '>' $fastaFile";
open(COM, "$grepcom |");
my $fastaSeqs = <COM>;
print $fastaSeqs, "\n<BR>";
close COM;

print "Counting sequences in Qual file $qualFile    ";
$grepcom = "/bin/grep -c '>' $qualFile";
open(COM, "$grepcom |");
my $qualSeqs = <COM>;
print $qualSeqs, "\n<BR>";
close COM;

        if($fastaSeqs == $qualSeqs)
{
    print "Great, number of sequences match\n<br>";
}
        else
{
            print "</font><font color=red>Error: number of sequences in fasta and qual file do not match, see numbers above. Aborting\n<B\
R></font>\n";
            exit;
}
my $com = "/home/mepweb/programs/translatePrimers.pl $primersFile > $dir/$i.primers";
open(COM, "$com |");
close COM;
$primersFile = "$dir/$i.primers";

# verify that primers are ok, read their length                                                                                   
print "Verifying primers file for $i...";
my $primersHashRef = &readPrimers($primersFile);
my @primers = values %{$primersHashRef};
 
