#!/usr/bin/env perl

use strict;
use warnings;

$|=1;

my ($inFile) = @ARGV;

unless ($inFile)
{
    die "
USAGE:
$0 inFile
";
}

open (IN, $inFile) || die "ERROR: can't open $inFile\n\a";

foreach my $line (<IN>)
{
	my $i=0;
    if ($line =~ /^>/){ print $line; }
	else
	{
		my @chars = split(//, $line);
		foreach my $char (@chars)
		{
			print $char;
			$i++;
			if($i >= 60)
			{
				print "\n";
				$i=0;
			}
		}
	}
}
close IN;
