#!/usr/bin/env perl

use strict;
use warnings;

$|=1;

my ($midFile, $mclFile) = @ARGV;

unless ($midFile && $mclFile)
{
    die "
USAGE:
$0 midFile mclFile
";
}


open (IN, $midFile) || die "ERROR: can't open $midFile\n\a";
my %clustersHash;
foreach my $line (<IN>)
{
    chomp $line;
    if ($line =~ /(\S+)/)
    {
	my @clusters = split /\s+/, $line;
	foreach my $cluster (@clusters)
	{
#	    my $sub_line = $line;
#	    $sub_line = s/\t$cluster//;
	    $clustersHash{$cluster}=$line;
	}
    }
}
close IN;

open (IN, $mclFile) || die "ERROR: can't open $mclFile\n\a";
foreach my $line (<IN>)
{
    chomp $line;
    if ($line =~ /(\S+)/)
    {
	my @sublines;
	my @clusters = split /\s+/, $line;
	foreach my $cluster (@clusters)
	{
	    if(exists $clustersHash{$cluster})
	    {
		push (@sublines, $clustersHash{$cluster});
	    }
	}
	my $line_out = join("\t", @sublines);
	print $line_out, "\n";
    }
}
close IN;
