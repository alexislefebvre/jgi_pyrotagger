#!/usr/bin/env perl

use strict;
use warnings;

my $qualFile = shift @ARGV;
unless($qualFile)
{
    die "
USAGE:
$0 qual_file
";
}

my $MAX_SEQ_SIZE=225;
my %badQualitySequences;
#my %goodSequences1;
my %goodSequences2;
#&readQualScores($qualFile);
&removeBadSeqs;

### compare the two hashes!
#my @goodSeqs1 = keys %goodSequences1;
#my @goodSeqs2 = keys %goodSequences2;

#print "Good 1:", scalar @goodSeqs1, "\n";
#print "good 2:", scalar @goodSeqs2, "\n";

sub readQualScores
{
    my $startTime = time;
    my $qualFile = shift @_;
    open (IN, $qualFile) || die "ERROR: can't open $qualFile\n\a";
    my $seqName;
    my $badBases=0;
    my $qualityThreshold=27;
    my $startCountingLength = 149;
    my $position=0;
    while (my $line =<IN>)
    {
        chomp $line;
        if ($line =~ />(\S+)/)
        {
            $seqName = $1;
            $badBases=0;
            $position=0;
        }
        else
        {
            my @lineArr = split /\s+/, $line;

            foreach my $qual_score (@lineArr)
            {
                $position++;
                if($qual_score <  $qualityThreshold)
                {
                    $badBases++;
                }
                my $qualitySequenceThreshold = int($position * 0.03)-1;
                if($badBases <= $qualitySequenceThreshold && $position > $startCountingLength)
                {
		    if($position==$MAX_SEQ_SIZE)
		    {
#			$goodSequences1{$seqName}=1;
		    }
                }
            }
        }
    }

    close IN;
    my $endTime = time;
    my $runningTime=$endTime - $startTime;
    print "Done in $runningTime seconds<BR>\n";
}


#my %goodSeqnences2;
sub removeBadSeqs
{
    if(-s $qualFile)
    {
        my $startTime = time;
        open (IN, $qualFile) || die "ERROR: can't open $qualFile\n\a";
        print STDERR "Reading Qual file\n";
        my $seqName;
#       my $badBases=0;                                                                                                      
        my @qualScores;
        my $qualityThreshold=27;
        my $badBasesThreshold=int($MAX_SEQ_SIZE * 0.03)-1;
        while (my $line =<IN>)
        {
            chomp $line;
            if ($line =~ />(\S+)/)
            {
                if ($seqName)
                {
                    chomp $line;
                    my $length = scalar @qualScores;
                    my $badBases = 0;
		    my @horribleBases;
                    for(my $i=0; $i<$MAX_SEQ_SIZE;$i++)
                    {
#			print $i, " ";
                        my $qualScore = $qualScores[$i];
                        unless(defined $qualScore) {  last; }
                        elsif($qualScore < $qualityThreshold)
                        {  
			    $badBases++;
			    push @horribleBases, $i;
			}
                    }
#		    <STDIN>;
                    if($badBases > $badBasesThreshold || $length < $MAX_SEQ_SIZE)
                    {
			$badQualitySequences{$seqName}=1;
#			if(exists $goodSequences1{$seqName})
##			{
#			    print "only in 1: ", $seqName, "\t", $badBases, "\t", join(" ", @horribleBases);
#			    <STDIN>;
#			}
		    }
		    else
		    {
#			unless(exists $goodSequences1{$seqName})
#                        {
#                            print "only in 2, length $length: ", $seqName, "\t", $badBases, "\t", join(" ", @horribleBases);
#                            <STDIN>;
#                        }
#			$goodSequences2{$seqName}=1;
		    }
		}
		$seqName = $1;
		@qualScores=();
#               $badBases=0;                                                                                                 
	    }
            else
	    {
		my @lineArr = split /\s+/, $line;
		push (@qualScores, @lineArr);
	    }
	}
	
        if ($seqName)
	{
	    my $badBases;
	    for(my $i=0; $i<$MAX_SEQ_SIZE;$i++)
	    {
		my $qualScore = $qualScores[$i];
                if($qualScore && $qualScore < $qualityThreshold)
                {
                    $badBases++;
                }
	    }
            if($badBases > $badBasesThreshold)
            {
                $badQualitySequences{$seqName}=1;
            }
	    else
	    {
#		$goodSequences2{$seqName}=1;
	    }
	    
	}
	close IN;
	my $endTime = time;
	my $runningTime=$endTime - $startTime;
	my $badSeqsNumber = scalar keys %badQualitySequences;
        print STDERR $badSeqsNumber, " sequences discarded because of low quality scores. This filtering took $runningTime seconds.\n";
    }
}
