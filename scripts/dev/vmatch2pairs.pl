#!/usr/bin/env perl

use strict;
use warnings;

$|=1;

my ($inFile) = @ARGV;

unless ($inFile)
{
    die "
USAGE:
$0 inFile

Input generation example:
/home/asczyrba/bin/vmatch -q 5461.99.5.fasta.clusters.seq.20  -d -l 217 -seedlength 27 -showdesc 0 -e 6 -nodist -noevalue -noscore -noidentity 5461.99.5.fasta.clusters.seq.20.index > 5461.99.5.fasta.clusters.seq.20.vmatch
";
}


open (IN, $inFile) || die "ERROR: can't open $inFile\n\a";

foreach my $line (<IN>)
{
    chomp $line;
    if ($line =~ /^\#/){next;}
    else{
	$line =~ s/^\s+//;
	my @fileds = split (/\s+/, $line);
	print $fileds[1], "\t", $fileds[5], "\n";
    }
}
close IN;

