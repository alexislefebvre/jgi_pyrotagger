#!/usr/bin/env perl

use strict;
use warnings;


$|=1;

my ($tableFile, $fastaFile) = @ARGV;

unless($tableFile && $fastaFile)
{
	die "
USAGE: $0 tableFile fastaFile
";
}


my %chimerasHash;
my %goodSeqsHash;

open (TABLE, $tableFile) || die "ERROR: can't open $tableFile\n";
my $chimera = 0;
foreach my $line (<TABLE>)
{
	if($line =~ /Potential chimeras:/){$chimera = 1;}
	elsif($line =~ /^Cluster(\d+)/)
	{
		if($chimera){$chimerasHash{$1}=1;}
		else{$goodSeqsHash{$1}=1;}
	}
}
close TABLE;

open (FASTA, $fastaFile) || die "ERROR: can't open $fastaFile\n";
my $seqName = "";
while (my $line=<FASTA>)
{
	if($line =~ />Cluster(\d+)/)
	{
		if(exists $goodSeqsHash{$1})
		{
			print $line;
			my $nextLine = <FASTA>;
			print $nextLine;
		}
		elsif(exists $chimerasHash{$1}){}
		else{die "ERROR: don't know what to do with Cluster$1, it's absent in the table.\n";}
	}
}
close FASTA;
