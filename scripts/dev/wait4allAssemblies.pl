#!/usr/bin/env perl

use strict;
use warnings;

$|=1;

my ($dir, $numberOfFiles) = @ARGV;

unless ($dir, $numberOfFiles)
{
    die "
USAGE:
$0 dir numberOfFiles
";
}

my $wait = 1;
WAIT: while($wait)
{
    FILES: for (my $i=1; $i <= $numberOfFiles; $i++)
    {	
	my $successFile = $dir . "trimmingDone.$i";
	unless (-e $successFile)
	{
	    sleep 1;
	    break FILES;
	}
	$wait = 0;
    }
}
