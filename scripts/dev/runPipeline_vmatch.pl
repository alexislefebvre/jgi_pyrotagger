#!/usr/bin/env perl

############################################################################################
#
# Pyrotags processing pipeline
#
# By Victor Kunin, vkunin@lbl.gov
#
# Feb 2009
#
############################################################################################

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Modueles -=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-

use strict;
use warnings;
use MIME::Lite;
use lib '/home/mepweb/programs';
use mapClusters;

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Parameters -=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-
my $USAGE= "
USAGE:
$0 params
Compulsory parameters:
-f file containing tab-delimited rows, in each row primer, fasta and/or qual files 
-p prefix for file names
-s minSeqSize 

Optional parameters:
-c file containing a concatenation of trimmed fasta files described in -f file
-t - use pyroclust algorithm and not vmatch
# -q qual file - if quality trimming needed
-d dir - run the program in dir directory
-e email - to send results over email

";

my ($fastaFile, $qualFile, $primersFile, $minSeqSize, $dir, $email, $pyroclust, $primers_and_fasta_list_file, $combined_fasta);
my ($file_names_file, $prefix, $finalQualFile);
#FIXIT on deployment.
my $programsDir  = "/home/mepweb/programs/";

while (my $arg = shift @ARGV)
{
    if($arg eq "-f"){$primers_and_fasta_list_file = shift @ARGV;}
    elsif($arg eq "-c"){$combined_fasta = shift @ARGV;}
    elsif($arg eq "-p"){$prefix     =shift @ARGV;}
    elsif($arg eq "-s"){$minSeqSize =shift @ARGV;}
    elsif($arg eq "-d"){$dir        =shift @ARGV;}
    elsif($arg eq "-e"){$email      =shift @ARGV;}
    elsif($arg eq "-t"){$pyroclust  = 1;}
    else {die $USAGE;}
}

unless($primers_and_fasta_list_file && $minSeqSize && $prefix){ die $USAGE;} 

$primersFile = $prefix . ".primers";

my $original_dir;
my $cluster_dir;

if($dir)
{
    $original_dir = $dir;
    $cluster_dir = "/scratch/" . "$$/";
    
    if(-e "/scratch/")
    {
	system "mkdir $cluster_dir";
	system "rsync -a $dir $cluster_dir";
	$dir = $cluster_dir;
	
    }
    chdir $dir;
    
}



unless($combined_fasta)
{
    $qualFile    = $prefix . ".fasta.qual";
    $fastaFile   = $prefix . ".fasta";

    if($dir)
    {
        $primersFile = $dir . "/" . $prefix . ".primers";
	$qualFile    = $dir . "/" . $prefix . ".fasta.qual";
	$fastaFile   = $dir . "/" . $prefix . ".fasta";
    }

    if(-e $primersFile || -e $fastaFile || -e $qualFile)
    {
	die "
ERROR:
Couldn't create one or more of the following files, file exists:
$fastaFile
$qualFile
$primersFile
";
    }
}
if(-e $primersFile)
{
    	die "
ERROR:
Couldn't create $primersFile file exists.
";
}

open(LIST, $primers_and_fasta_list_file)|| die "ERROR: can't open $primers_and_fasta_list_file\n";
my $fileFound;
my $trimmedPrimerFastaMapFile = $dir . "/". $prefix . "_primers2fasta.trimmed";
open (TrimmedMap, "> $trimmedPrimerFastaMapFile") || die "ERROR: can't write to $trimmedPrimerFastaMapFile\n";
my $i=0;
if($combined_fasta)
{
    print STDERR "Assuming that you already done the trimming and wabble resolution\n";
}

foreach my $line (<LIST>)
{
    $i++;
    chomp $line;
    my ($primers, $fasta, $qual) = split ("\t", $line);
    if($primers && $fasta)
    {
	my $primers_unwabbled;

	unless($combined_fasta) # assuming that if you have a combined fasta, you already done the wabble thing.
	{
	    $primers_unwabbled  = $dir . "/" . $prefix . "." . $i . ".primers";
	    my $com = $programsDir . "translatePrimers.pl $primers > $primers_unwabbled";
#	    &executeCom($com);
	    open(COM, "$com |");
	    close COM;
	}
	else
	{
	    $primers_unwabbled = $primers;
	}
	my $com = "cat $primers_unwabbled >> $primersFile";
	system $com;
#	&executeCom($com);
	print TrimmedMap $primers_unwabbled, "\t";
	$fileFound=1;
    }
    if($combined_fasta)
    {
	undef $qualFile;
	$fastaFile=$combined_fasta;
	print TrimmedMap $fasta, "\n";
#	$fileFound=1;
    }
    elsif($qual)
    {
	my $trimmedFasta = &lucy_trim($fasta, $qual);
	print TrimmedMap $trimmedFasta, "\n";
	my $com    = "cat $trimmedFasta  >> $fastaFile";
	&executeCom($com);
	my $finalQualFile = $fastaFile . ".qual";
	my $trimmedQual = $trimmedFasta . ".qual";
	$com    = "cat $trimmedQual >> $finalQualFile";
	&executeCome($com);
    }
    elsif($fasta)
    {
	print TrimmedMap $fasta, "\n";
	undef $qualFile;
	my $com    = "cat $dir/$fasta  >> $fastaFile";
	&executeCom($com);
        my $trimmedQual = $dir . "/" . $fasta . ".qual";
	print STDERR "Checking for presence of $trimmedQual ...";
	my $finalQualFile = $fastaFile . ".qual";
	if(-s $trimmedQual)
	{
	    print STDERR "present\n";
	    my $com    = "cat $trimmedQual >> $finalQualFile";
	    &executeCom($com);	
	}
	else {print STDERR "absent\n";}
    }
}
close TrimmedMap;
close LIST;
#exit;

unless($fileFound)
{
    die "ERROR: could not see primers and fasta files\n";
}


#-=-=-=-=-=-=-=-=-=-=--=-=-=-=--=-=-=-=- Constants -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

## FIXIT here:
my $trimCluster  = $programsDir . "trimClusterSeqs.pl";
my $filterCommand = $programsDir . "filterByIdentity.pl";
my $getFastaCommand = $programsDir . "getClusterReps4Superclusters.pl";
my $produceTableCommand = $programsDir . "produceTable.pl";
my $lucyPrep      = $programsDir . "lucyTrim.sh";
my $pyroclustProg = $programsDir . "align_pyrotags.pl";

my $vmatchDir = '/home/asczyrba/bin/';
my $mkVtree   = $vmatchDir . 'mkvtree -dna -pl -allout -v ';
my $vmatch    = $vmatchDir . 'vmatch -d ';

my $clustersFile      = $fastaFile . ".clusters";
my $seqsFile          = $fastaFile . ".clusters.seq";
my $superClustersFile = $prefix . ".superclusters";
my $repsFile          = $prefix . ".reps.fasta";
my $clusters2reads    = "clusters2reads.txt";
my $clusters2readsChimeras = "clusters2reads.chimeras.txt";
my $tableFile         = "cluster_classification.xls";
my $fractions_file    = "cluster_classification_percent.xls";
my $phyloGraphFile    = "phylum_classification.xls";
my $phyloGraphFractionsFile = "phylum_classification_percent.xls";

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Workflow -=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-

$|=1;

my $primersHashRef = &readPrimers($primersFile);
my $primerLength;
{
    my @primers = keys %{$primersHashRef};
    $primerLength = length $primers[0];
}
my $seqWithoutPrimerSize = $minSeqSize - $primerLength;
my $divergence = 0.03;
my $seedLength = 33;
my $mismatches = int($seqWithoutPrimerSize * $divergence);
my $perc_identity = 100*(1-$divergence);
my $vtreeClusters = $clustersFile . ".vmatch_clusters";

&runClustering;


#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Subroutines -=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-

#-=-=-=-=-=-=--

#
# Step-by step clustering menagment.
#
sub runClustering
{
#    if($qualFile){	&lucy_trim;    }

    # first clustering stage; identify identical sequences;
    my $trimCom = $trimCluster . " $fastaFile $primersFile $clustersFile $seqsFile $minSeqSize ";
    &executeCom ($trimCom);

    ### check if the output file is empty; abort if it is
    my $clusters_file_size = -s $clustersFile;
    my $seqs_file_size = -s $seqsFile;
    unless(-s $clustersFile && $seqsFile)
    {
	&abort_execution("Failed to cluster. May be your primers do not match the dataset?");
	exit;
    }	
    
    # run second level clustering, depending on the algorithm
    unless($pyroclust){    &runVmatch;}
    else{&runPyroclust}

    # classify clusters to the nearest neibour in the phylogenetic database
    my $phyloBlastFile = $repsFile . ".phyloblast";
    my $phyloBlastDatabase = "/home/mepweb/referenceDb/pyrotag_bac_arc_euk_db.fa.select";
    my $phyloBlastCom = "/jgi/tools/bin/blastall -p blastn -W 90 -i $repsFile -b 1 -m 8 -o $phyloBlastFile -d $phyloBlastDatabase ";
    &executeCom($phyloBlastCom);
    
    # select sequences that did not have hits; fetch them and re-blast; add the results to the final thing
    my $seqsWithoutHitsFile = $phyloBlastFile. ".nohits";
    my $selectUnblastedSeqs = $programsDir . "selectUnblastedSeqs.pl  $phyloBlastFile $repsFile > $seqsWithoutHitsFile";
    &executeCom($selectUnblastedSeqs);

    my $reblastCom = "/jgi/tools/bin/blastall -p blastn -W 30 -i $seqsWithoutHitsFile -b 1 -m 8 -d $phyloBlastDatabase >> $phyloBlastFile";
    &executeCom($reblastCom);

    # blast output will contain only fragments of sequences - complement to the complete name
    my $giveNamesFile = $phyloBlastFile . ".names";
    my $giveNamesCom = $programsDir . "mapBlastOutputHist2database.pl $phyloBlastDatabase $phyloBlastFile > $giveNamesFile";
    &executeCom($giveNamesCom);

    my $potentialChimerasFile = $phyloBlastFile . ".chimeras";
    my $chimeraCheckCommand = $programsDir . "chimeraCheck.pl $phyloBlastFile $seqWithoutPrimerSize > $potentialChimerasFile";
    &executeCom($chimeraCheckCommand);

    # make a table, in which columns are experiments separated by primers/barcodes
    #                    and rows are OTUs, or clusters
#    my $produceTableCom = "$produceTableCommand $finalFasta $mclFile $clustersFile $primersFile $fastaFile $giveNamesFile > $tableFile";
    my $produceTableCom = "$produceTableCommand $repsFile $superClustersFile $clustersFile $giveNamesFile $trimmedPrimerFastaMapFile $potentialChimerasFile > $tableFile";
    &executeCom($produceTableCom);
    my $clusters2reads = $repsFile . ".clusters2reads"; 

    my $parseTableCom = $programsDir . "parseTable.pl $tableFile 2 r $phyloGraphFile $phyloGraphFractionsFile";
    &executeCom($parseTableCom);
    # if email is present
    if($email){&compressAndSendEmail;}
    else{	&copyFilesAndCleanUp;}
}
 


#-=-=-= Other subroutines

sub executeCom
{
    my $com = shift @_;
#    print STDERR $com, "\n";
#    $com = "/usr/bin/time " . $com;
    print STDERR "Executing $com\n";
    my $startTime = time;
    open (COM, "$com |") || die "ERROR: can't execute $com\n";
    print STDERR <COM>;
    close COM;
    my $endTime = time;
    my $time_diff = $endTime - $startTime;
    print STDERR "Done in $time_diff seconds\n";
}

sub runVmatch
{
    my $vtreeDbIndex  = $seqsFile . ".index";

    # see if the scratch directory exists in the filesystem
    # if yes, run vmatch there.
    my $seqFileScratch =  $seqsFile;
    if(-e "/scratch/")
    {
	my $tmp_dir = "/scratch/$$";
	my $mkdir_com = "/bin/mkdir $tmp_dir";
	&executeCom($mkdir_com);
	$tmp_dir .= "/";
	$seqFileScratch = $tmp_dir . "seqs";
	$vtreeDbIndex=$seqFileScratch . ".index";
	my $copyFileCom =  "cp $seqsFile $seqFileScratch";
	&executeCom($copyFileCom);
    }


    # index the database
    my $vtreeCom = $mkVtree . " -indexname " . $vtreeDbIndex . " -db " . $seqFileScratch;
    &executeCom($vtreeCom);

    my $vtreeOutput = $seqFileScratch . ".vmatch";
    # run vmatch
#    my $vmatchCom = $vmatch . "-l " . $seqWithoutPrimerSize . " -seedlength " . $seedLength . " -showdesc 0 -e $mismatches -dbcluster $perc_identity $perc_identity $vtreeDbIndex > $vtreeClusters";
    my $vmatchCom = $vmatch . "-l " . $seqWithoutPrimerSize . " -seedlength " . $seedLength . " -q $seqFileScratch  -nodist -noevalue -noscore -noidentity -showdesc 0 -e $mismatches $vtreeDbIndex > $vtreeOutput";
    &executeCom ($vmatchCom);

    my $vtreeMclLikeFile = $vtreeOutput . ".4mcl";
    my $reformat4mclCommand = $programsDir . "vmatch2pairs.pl $vtreeOutput > $vtreeMclLikeFile";
    &executeCom ($reformat4mclCommand);
    
    # run MCL
    my $mclCom = "/usr/bin/mcl $vtreeMclLikeFile --abc -o $superClustersFile";
    &executeCom ($mclCom);

    # recover the reps file
    my $getFastaCom = $programsDir . "getClusterReps4Superclusters.pl $superClustersFile $seqsFile > $repsFile";
    &executeCom($getFastaCom);

    # reformat vmatch output to be mcl-like; select sequence reps for clusters
#    my $readVmatchCom = "/home/mepweb/programs/getBestVmatchReps.pl $seqsFile $vtreeClusters $repsFile $superClustersFile";
#    &executeCom($readVmatchCom);

    my $removeIndexCom;
    if(-e "/scratch/$$/")    {	$removeIndexCom = "rm -r /scratch/$$";    }
    else{ $removeIndexCom = "rm $vtreeDbIndex.* $vtreeMclLikeFile $vtreeOutput";}
    &executeCom($removeIndexCom);
}

sub runPyroclust
{
    # run align_pirotags script
    my $large_word_size;
    if($minSeqSize < 300)    {	$large_word_size = 80;    } # FLX run
    else{ $large_word_size = 150;  } # titanium rin

    my $alignPyrotagsCom = $pyroclustProg . " $seqsFile $large_word_size 6 0.97";
    my $alignedPyrotagsClusters = $seqsFile . ".pyroclusters";
    my $finalFasta = $alignedPyrotagsClusters . ".seq";
    &executeCom($alignPyrotagsCom);
    rename ($finalFasta, $repsFile);
    rename ($alignedPyrotagsClusters,$superClustersFile);
}


sub lucy_trim
{
    my ($fastaFile, $qualFile)=@_;
    ############
    #
    # Do lucy-trimming

    my $lucy_trim_com = $programsDir . "runLucyTrim.pl $fastaFile $qualFile 99.8";
    &executeCom($lucy_trim_com);

    # from now on, the fasta file is the lucy-trimmed fasta
    # and we need to tell it to the other programs too!
    
    $fastaFile = $fastaFile . ".99.8.fasta";

    # check if the output file is empty; abort if it is
    my $outputFastaFileSize = -s $fastaFile;
    unless(-s $fastaFile)
    {
	&abort_execution("Failed to lucy-trim your data. Please verify your input data files");
	exit;
    }
    return $fastaFile;
}

sub compressAndSendEmail
{
    # files to send to user:
    #    final table
    #    cluster representative sequences
    #    maping of final clusters to reads
#    my $zipCom = "/bin/gzip $clusters2reads $tableFile $repsFile $fractions_file $clusters2readsChimeras ";
    my $zipCom = "/bin/gzip $clusters2reads $tableFile $repsFile $fractions_file  ";

    &executeCom($zipCom);
#my $fractions_file = ".classification_fraction.xls"    
    # mail the ziped file to the user if email is present
    my $msg = MIME::Lite->new(
			      From    => 'vkunin@lbl.gov',
			      To      => $email,
			      Subject => 'Pyrotag processing results',
			      Type    => 'multipart/mixed',
			      );
    $msg->attach(
		 Type     => 'TEXT',
		 Data     => "
Dear User,

Here are your pyrotag processing results from run $prefix. Please find attached 5 files. Some files are compressed using gzip program, you will need a file compression software to uncompress them.

The files cluster_classification.xls and cluster_classification_percent.xls contain mapping of clusters (OTUs clustered at 97% identity) to datasets (per read or as dataset percent fractions respectively), as identified in your primer/barcode file. For each cluster the best hit in greengenes (for prokaryotes) and silva (for eukaryotes) databases is given, together with Blast alignment information in a concise (m8) format. Potential chimeras, if any, are specified in the bottom of these files (look for 'Potential chimeras').

The file phylum_classifications.xls and phylum_classification_percent contain the distribution of read counts for each phylum found in each of your datasets (per read or as dataset percent fractions respectively).

The file clusterRepresentatives.fasta contains representative sequences for each cluster.

The file clusters2reads.txt maps which reads were assigned to which cluster.

Sincerely yours, 

      The PyroTagger team.
",
		 );
    $msg->attach(
		 Type     => 'application/x-gzip',
		 Path     => $tableFile . ".gz",
#		 Filename => "classification.xls.gz",
		 Disposition => 'attachment'
		 );
    $msg->attach(
		 Type     => 'application/x-gzip',
		 Path     => $fractions_file . ".gz",
#		 Filename => "classification_percent.xls.gz",
		 Disposition => 'attachment'
		 );
    $msg->attach(
		 Type     => 'application/x-gzip',
		 Path     => $repsFile . ".gz",
		 Filename => "clusterRepresentatives.fasta.gz",
		 Disposition => 'attachment'
		 );
    $msg->attach(
		 Type     => 'application/x-gzip',
		 Path     => $clusters2reads . ".gz",
		 Filename => "clusters2reads.txt.gz",
		 Disposition => 'attachment'
		 );    
#    $msg->attach(
#                 Type     => 'application/x-gzip',
#                 Path     => $clusters2readsChimeras . ".gz",
#                 Filename => "clusters2reads.chimeras.txt.gz",
#                 Disposition => 'attachment'
#		 );
    $msg->attach(
                 Type     => 'application/msexcel',
		 Path     => $phyloGraphFile,
#		 Filename => "phylograph.xls",
		 Disposition => 'attachment'
		 );
    $msg->attach(
                 Type     => 'application/msexcel',
		 Path     => $phyloGraphFractionsFile,
#		 Filename => "phylograph.xls",
		 Disposition => 'attachment'
		 );
    $msg->send;
    &copyFilesAndCleanUp;
}


sub abort_execution
{
    my ($error_message) = @_;
    if ($email)
    {
	my $sendmail = "/usr/sbin/sendmail ";
	my $subject = "Subject: Pyrotags processing failed";
#	my $file = "subscribers.txt"; 

	my $msg = MIME::Lite->new(
				  From    => 'vkunin@lbl.gov',
				  To      => $email,
				  Subject => 'Pyrotag processing failed',
				  Type    => 'multipart/mixed',
				  );
	$msg->attach(
		     Type     => 'TEXT',
		     Data     => "Your submission to pyrotags pipeline failed.\nReason: $error_message",
		     );

	$msg->send; 
	exit;
    }
    else
    {
	&copyFilesAndCleanUp;
	die $error_message;
    }
}

sub copyFilesAndCleanUp
{
    if($dir && $original_dir)
    {
	my $errors_file = $cluster_dir . "cluster.errors";
	my $stdout_file = $cluster_dir . "cluster.stdout";
	unlink $errors_file;
	unlink $stdout_file;
	system "rsync -q -a $cluster_dir $original_dir";
	system "rm -rf $cluster_dir";
    }
}
