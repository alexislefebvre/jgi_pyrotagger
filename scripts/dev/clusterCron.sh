#!/bin/bash

/usr/bin/stat /opt/sge/hyperion > /dev/null # make sure hyperion is mounted.
source "/opt/sge/hyperion/common/settings.sh" # set up the cluster environment

FILE=${1:?"Specify input file to look at"} # write to this file commands to execute

for (( ; ; ))
do 
    while read LINE
    do
    echo $LINE
    $LINE
    done < $FILE
    rm $FILE
    touch $FILE
    chmod 666 $FILE
    sleep 1
done

