#!/usr/bin/env perl

use strict;
use warnings;

$|=1;

my ($fastaFile, $accFile) = @ARGV;

unless ($fastaFile && $accFile)
{
    die "
USAGE:
$0 fastaFile accFile
";
}

open (IN, $accFile) || die "ERROR: can't open $accFile\n\a";
my %accHash;
foreach my $line (<IN>)
{
    chomp $line;
    if ($line =~ /(\S+)/)
    {
	$accHash{$1}=1;
    }
}
close IN;

open (IN, $fastaFile) || die "ERROR: can't open $fastaFile\n\a";
my $printLine;
foreach my $line (<IN>)
{
    chomp $line;
    if ($line =~ />(\S+);/)
    {
	my $seqId = $1;
	if($seqId =~ /\d+/)
	{
	    if(exists $accHash{$seqId})
	    {
		print $line, "\n";
		$printLine =1;
	    }
	    else{$printLine = 0;}

	}
	else
	{
	    print $line, "\n";
	    $printLine =1;
	}
    }
    elsif($printLine)
    {
	print $line, "\n";
    }
}
close IN;

