#!/usr/bin/env perl

use strict;
use warnings;

$|=1;


################################################################################################################
#
#
# A program for aligning a set of sequences which are highly similar and of identical length
#
# By Victor Kunin, vkunin@lbl.gov, March 2009.
#
# Works by looking up 
#         large words shared between sequences, then 
#         small words shared between sequences, then 
#         performs a slightly optimized Needlman-Wunch global alignment
#
# Ouptuts set of sequences within determined % identity.
# Note that transitivity is not upheald. That is if there is A-B-C but not A-C, A and B will be clustered, C will be separate.
#
################################################################################################################

#-=-=-=-=-=-=-=-=-=--==--=-=-=-=-=--=-=-=-=-=-=-=-=- Define global variables -=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-

my $SEQ_LENGTH;
my $mismatches_number;
my $short_words_number_per_sequence;
my $long_words_number_per_sequence; 
my $matches_threshold;
my $short_word_mismatches;

my @global_seqs;

my %seqHash;
my %longWordsHash;
my %shortWordsHash;
my %clusterHash;
my %finalClustersHash;
my %dont_align;
my %seqs2skip;

my $startTime = time;

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=  Parameters -=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

my ($inFile, $long_word_length,$short_word_length,$identity, $dump_data) = @ARGV;

my $USAGE = "
USAGE:
$0 inFile long_word_length short_word_length identity [dump_data]

dump_data causes the program to exit after first clustering step and dump inclomplete clusters. Assumes re-clustering with some other tool (such as vmatch).
";

unless ($inFile && $long_word_length && $short_word_length && $identity)
{
    die $USAGE;
}

if($identity > 1 || $identity < 0)
{
    die $USAGE, "Identity must be between 0 and 1\n";;
}

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=  Work flow -=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


my $count = 0;
my $alignCount=0;
&read_file;

my $new_subclusters_number = @global_seqs;

my $midTime = time;

my $timeDiff = $midTime - $startTime;
print STDERR "\nFirst stage done in $timeDiff seconds\n";

if($dump_data)
{
    my $midClusters = $inFile . ".midclusters";
    my $midClustersSeq = $inFile . ".midclusters.seq";
    open(MIDC, "> $midClusters") || die "ERROR: can't open $midClusters for writing.\n";
    open(MIDCS, "> $midClustersSeq") || die "ERROR: can't open $midClustersSeq for writing.\n";
    foreach my $seqName (@global_seqs)
    {
	print MIDCS ">$seqName\n";
	print MIDC $seqName;
	print MIDCS $seqHash{$seqName}, "\n";
	foreach my $seq1 (keys %{$clusterHash{$seqName}})
	{
	    print MIDC "\t", $seq1;
	}
	print MIDC "\n";
    }
    close MIDC;
    close MIDCS;

    exit;
}


&refine_subclusters;
my $endTime = time;
$timeDiff = $endTime - $midTime;
print STDERR "\nSecond stage took $timeDiff seconds\n";

my $output_cluster_file = $inFile . ".pyroclusters";
my $output_sequence_file = $output_cluster_file . ".seq";

open (CLUSTERS, "> $output_cluster_file") || die "ERROR: can't open $output_cluster_file for writing\n";
open (SEQS, "> $output_sequence_file") || die "ERROR: can't open $output_sequence_file for writing\n";
# Printing cluster info and cluster reps

$count = 0;


foreach my $seqName(@global_seqs)
{
    if(exists $finalClustersHash{$seqName})
    {
	$count++;
	print CLUSTERS $seqName;
	print SEQS ">$seqName\n";
	foreach my $seq2 (keys %{$finalClustersHash{$seqName}})
	{
	    print CLUSTERS "\t", $seq2;
	}
	print SEQS $seqHash{$seqName}, "\n";
	print CLUSTERS "\n";
    }
}
close CLUSTERS;
close SEQS;
print STDERR "\n", $count . " clusters saved\n";



#-=-=--=-=-=-=--=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=- Subroutines -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


sub refine_subclusters
{
    print STDERR "\nRefining $new_subclusters_number clusters\n";
    $count=0;

    my $i = $#global_seqs;
    while($i>-1)
    {
	my $seqName=$global_seqs[$i];
	$i--;
	$count++;
	if ($count/10 == int($count/10))
	{
	    print STDERR "\r", $count/10, "0";
	}
	
	my $match_found = &find_short_maches($seqName, \@global_seqs,\%shortWordsHash, \%finalClustersHash, $short_word_length);
	if($match_found)
	{
	    foreach my $seq (keys %{$clusterHash{$seqName}})
	    {
		$finalClustersHash{$match_found}{$seq}=$clusterHash{$seqName}{$seq};
	    }
	    if(exists $finalClustersHash{$seqName})
	    {
		foreach my $seq (keys %{$finalClustersHash{$seqName}})
		{
		    $finalClustersHash{$match_found}{$seq}=$finalClustersHash{$seqName}{$seq};
	    }
		delete $finalClustersHash{$seqName};
	    }
	}
	else
	{
	    unless(exists $finalClustersHash{$seqName})
	    {
		$finalClustersHash{$seqName}=();
	    }
	    if(exists $clusterHash{$seqName})
	    {
		foreach my $seq (keys %{$clusterHash{$seqName}})
		{
		    $finalClustersHash{$seqName}{$seq}=$clusterHash{$seqName}{$seq};
		}
	    }
	}
	$seqs2skip{$seqName}=1;
    }
}


sub read_file
{
    print STDERR "Reading sequences\n";
    open (IN, $inFile) || die "ERROR: can't open $inFile\n\a";
    my $constants_set = 0;
    my $seqName;
    my $seq;
    while (my $line = <IN>)
    {
	chomp $line;
	if ($line =~ />(\S+)/)
	{
	    if($seqName)
	    {
		unless($constants_set)
		{ 
		    &set_constants(length $seq);
		    $constants_set=1;
		}
		&assign_seq($seqName,$seq)
	    }
	    $seqName = $1;
	    $seq = "";
	}
	else
	{
	    $seq .= $line;
	}
    }
    close IN;

    if($seqName)
    {
	&assign_seq($seqName,$seq)
    }

}

sub set_constants
{
    ($SEQ_LENGTH) = @_;

    $mismatches_number = int($SEQ_LENGTH * (1 - $identity));
    $short_word_mismatches = $mismatches_number * $short_word_length;
#    print STDERR "Seqlength: $SEQ_LENGTH\tmismatches\t";
#    print STDERR $mismatches_number;
#    <STDIN>;
    $short_words_number_per_sequence = $SEQ_LENGTH - $short_word_length +1;
    $long_words_number_per_sequence = $SEQ_LENGTH  - $long_word_length +1;
    $matches_threshold = $short_words_number_per_sequence - $mismatches_number * $short_word_length;
}


sub assign_seq
{
    my ($seqName, $seq) = @_;
    $seqHash{$seqName}=$seq;
    &hash_words($seqName, $seq);
}

sub hash_words
{
    my ($seqName, $seq) = @_;
    $count++;
    if ($count/100 == int($count/100))
    {
	print STDERR "\r", $count/100, "00";
    }

    my $seqLength = length $seq;
    my @short_words;
    my @long_words;
    my %short_words;
    my %long_words;
    for (my $i =0; $i < $short_words_number_per_sequence; $i++)
    {
	my $short_word = substr ($seq,$i, $short_word_length);
	$short_words{$seqName}{$short_word}++;
	
	push @short_words, $short_word;
    }
    for (my $i =0; $i < $long_words_number_per_sequence; $i++)
    {
	my $long_word = substr ($seq,$i, $long_word_length);
	push @long_words, $long_word;
    }

    my %matches2seq;
    my %long_matches2seq;
    foreach my $long_word(@long_words)
    {    
	my @long_word_seqs = keys %{$longWordsHash{$long_word}};

	foreach my $seqName2(@long_word_seqs)
	{
	    $long_matches2seq{$seqName2}++;
	}
    }
    my @long_matches = keys %long_matches2seq;
    @long_matches = sort {$long_matches2seq{$b} <=> $long_matches2seq{$a}} @long_matches;
 
    my $match_found = &find_short_maches($seqName, \@long_matches, \%short_words, \%clusterHash, $short_word_length);

    unless($match_found)
    {
	 foreach my $short_word(@short_words)
	 {
	     $shortWordsHash{$seqName}{$short_word}++;
	 }
	 foreach my $long_word(@long_words)
	 {
	     $longWordsHash{$long_word}{$seqName}++;
	 }
	push(@global_seqs, $seqName);  
    }

}

sub find_short_maches
{
    my ($seqName, $candidatesRef, $short_wordsRef, $clustersHash2useRef, $word_length)=@_;

    my @short_words = keys %{$$short_wordsRef{$seqName}};
    my $match_found=0;
    seq: foreach my $seqName2 (@$candidatesRef)
    {
	if(exists $seqs2skip{$seqName2}){next;}
	if($seqName eq $seqName2){next;}

	my $mismatches=0;
	my $matches;
	word: foreach my $short_word(@short_words)
	{
	    if(exists $shortWordsHash{$seqName2}{$short_word})
	    {
		$matches+=&min($$short_wordsRef{$seqName}{$short_word}, $shortWordsHash{$seqName2}{$short_word});
	    }
	    else
	    {
		$mismatches++;
		if($mismatches > $short_word_mismatches)
		{next seq;}
	    }
	}
	
	if($mismatches_number <= $word_length && $mismatches < $mismatches_number) # up to $mismatches_number mismatches, cluster without alignment
	{
	    $$clustersHash2useRef{$seqName2}{$seqName}=1;
	    $match_found=$seqName2;
	    last seq;
	}
	elsif($matches >= $matches_threshold )
	{
	    unless(exists $dont_align{$seqName}{$seqName2})
	    {
		my $d = &Needleman_Wunsch($seqHash{$seqName},$seqHash{$seqName2});
#		print STDERR "$seqName\t$seqName2\t$d\n";
#		<STDIN>;
		unless($d > $mismatches_number)
		{
		    $$clustersHash2useRef{$seqName2}{$seqName}=1;
		    $match_found=$seqName2;
		    last seq;
		}
		else
		{
		    $dont_align{$seqName}{$seqName2}=$d;
		    $dont_align{$seqName2}{$seqName}=$d;
#		    last word;
		}
	    }
#	    else {print "skipping $seqName\t$seqName2\t$dont_align{$seqName}{$seqName2}\n";}
	}
#	else	{	    print "disqualified $seqName\t$seqName2\t$matches\n";	}
	
    }
    return $match_found;
}



###########
#
# Implements Needleman-Wunsh algorithm, with a speedup for aborting when divergence is higher than maximum.
#
#
sub Needleman_Wunsch
{
    my ($seq1, $seq2) = @_;

    my @arr1 = split "", $seq1;
    my @arr2 = split "", $seq2;
    my $seqLength = scalar @arr1;

    my $too_many_mismatches = $mismatches_number + 2;
    my $best_i =$too_many_mismatches;
    my $best_j = $too_many_mismatches;

    my @f_array;
    for (my $i=0; $i < $seqLength; $i++)
    {
	$f_array[$i][0]=$i;
	$f_array[0][$i]=$i;
    }
    for (my $i=1; $i <= $seqLength; $i++)
    {
	my $min_i =$too_many_mismatches;
	for(my $j=$i-$mismatches_number; $j <= $i+$mismatches_number  && $j <= $seqLength ; $j++)
	{    
	    if($j < 1 || $j>  $seqLength){next;}
	    
	    my $subst = 0;
	    if($arr1[$i-1] ne $arr2[$j-1])
	    {
		$subst = 1;
	    }
	    my($d1,$d2,$d3) = ($too_many_mismatches,$too_many_mismatches,$too_many_mismatches);

	    if(defined $f_array[$i-1][$j-1]){ $d1 = $f_array[$i-1][$j-1] + $subst;}
	    if(defined $f_array[$i-1][$j]){ $d2 = $f_array[$i-1][$j]+1;}
	    if(defined $f_array[$i][$j-1]){ $d3 = $f_array[$i][$j-1]+1;}
	    my $min_j =&min($d1,$d2,$d3);
	    unless($min_j > $mismatches_number)
	    {
		$f_array[$i][$j]=$min_j;
	    }
	    if($j == $seqLength && $best_j > $min_j){$best_j = $min_j;}

	    if($min_i > $min_j)
	    {
		$min_i = $min_j;
	    }
	}
	if($i == $seqLength){$best_i = $min_i;}

	if($min_i > $mismatches_number){last;}# return $min_i;}
    }
#    for (my $i=0; $i <= $seqLength; $i++)
#    {
#         for(my $j=0; $j <= $seqLength; $j++)
#        {
#	    if(exists $f_array[$i][$j])
#	    { print $f_array[$i][$j];}
#	    print "\t";
#	}
#	print "\n";
#    }
#    print STDERR $f_array[$seqLength][$seqLength];
#    <STDIN>;
#    print STDERR "min: ", &min($best_i, $best_j);
#    exit;
    #compute the alignment
#    &compute_alignment(\@f_array,$seqLength,$seqLength);
    return &min($best_i, $best_j);
#    return $f_array[$seqLength][$seqLength];
}

sub min
{
    my ($min) = @_;
    foreach my $item(@_)
    {
	if ($item < $min){$min = $item;}
    }
    return $min;
}

sub compute_alignment
{
    my ($f_array_ref,$seq1Length,$seq2Length) = @_;

    my @f_array = @$f_array_ref;

    my $alA = '';
    my $alB = '';

    my $i=$seq1Length;
    my $j=$seq2Length;

    while($i > 0 && $j >0)
    {
	my $score = $f_array[$i][$j];
	my $scoreDiag = $f_array[$i-1][$j-1];
	my $scoreUp = $f_array[$i][$j-1];
	my $scoreLeft = $f_array[$i-1][$j];
	my $subst = 0;
#	if($arr1[$i-1] ne $arr2[$j-1])
#	{
#	    $subst = 1;
#	}
#	if($score == $scoreDiag + $subst)
#	{
#	    $alA = $$alA
#	}
    }
}
