#!/usr/bin/env perl

use strict;
use warnings;
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use FindBin qw($Bin);
use lib "$Bin/../../perl/share/perl/5.10.1";
use Config::Simple;

# CONFIG
my %Config;
Config::Simple->import_from( "$Bin/../../pyrotagger.ini", \%Config );
die("Outputs dir not defined\n") unless exists( $Config{output_dir} );
die("Outputs dir does not exist\n") unless -d $Config{output_dir};
my $outputs_dir = $Config{output_dir};

#my $filesDir = "/tmp/";

my $fileType = param('fileType');
my $fileName = param('fileName');
my $id       = param('id');

#die("fileType not defined\n") unless $fileType;
die("fileName not defined\n") unless $fileName;
die("id not defined\n")       unless $id;

my $dir               = "$outputs_dir/$id/";
my $clean_directories = 0;
if ($fileName)
{

	my $error_message;
	if (( $fileName =~ /\.errors$/ ) or ( $fileName =~ /_errors$/ ))
	{
		print "Content-Type: text/html;\n\n";
		print
		  "<HTML><HEAD><meta http-equiv=\"refresh\" content=\"10\"></HEAD><BODY><pre>";
		$clean_directories = 1;
		$error_message     = "This run did not start yet.<BR>\n";
	}
	elsif ( $fileName =~ /.gz$/ )
	{
		print "Content-Type: application/x-gzip;\n";
		print "Content-Disposition: attachment; filename=$fileName\n\n";
	}
	elsif ( $fileName =~ /.xls/ )
	{
		print "Content-Type: application/vnd.ms-excel;\n";
		print "Content-Disposition: attachment; filename=$fileName\n\n";
	}
	else
	{
		$error_message = "ERROR: can't open $fileName";
		print "Content-Type: text/plain;\n\n";
	}

	my $file = $dir . $fileName;

	unless ( -e $file )
	{
		&printdie("Content-Type: text/plain;\n\nFile $file doesn't exit");
	}

	open( FILE, $file ) || &printdie($error_message);

	if ($clean_directories)
	{
		while ( my $line = <FILE> )
		{
			$line =~ s/$outputs_dir//g;
			my @win_lines = split( "\r", $line );
			my $last_line = $win_lines[$#win_lines];
			$last_line =~ s/\r//g;
			print $last_line;
		}
		print <FILE>;
		if ( $fileName =~ /\.errors$/ )
		{
			print "\n</pre></body></html>";
		}
	}
	else
	{
		binmode FILE;
		binmode STDOUT;

		while ( read( FILE, my $buff, 8 * 2**10 ) )
		{
			print STDOUT $buff;
		}
	}

	close FILE;

}
elsif ( $fileType && $fileType eq 'png' )
{
	my $fileName = "$outputs_dir/reads_distribution.png";
	my $q        = new CGI;
	open( my $DLFILE, '<', "$fileName" ) or return (0);
	print $q->header( -type => 'image/png', -expires => "now" );
	binmode $DLFILE;
	print while <$DLFILE>;
	undef($DLFILE);

}
else
{
	&printdie(
		"Content-Type: text/plain;\n\nPlease provide an argument for name of the file to download."
	);
}

sub printdie
{
	print @_;
	exit;
}
