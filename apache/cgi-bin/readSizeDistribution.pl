#!/usr/bin/env perl

use strict;
use warnings;
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use FindBin qw($Bin);
use lib "$Bin/../../perl/share/perl/5.10.1";
use Config::Simple;
use GD::Graph::lines;

# CONFIG
my %Config;
Config::Simple->import_from( "$Bin/../../pyrotagger.ini", \%Config );
die("Outputs dir not defined\n") unless exists( $Config{output_dir} );
die("Outputs dir does not exist\n") unless -d $Config{output_dir};

my $filesDir = $Config{output_dir};

#my $fileType = param('fileType');
my $id            = param('id');
my $dumpHash      = param('dumpHash');
my $readHash      = param('readHash');
my $submittedFile = param('fastaFile1');
my $minSize2Show  = param('minSize');
my $minSize       = 9999999999999;
my $maxSize       = 0;
my $maxY          = -1;
my %hash2print;
my $hashFile;
my $nothing;
my @fastaFiles;
my $fastaFile;
my @xVals;
my %yVals;

if ($id)
{
	$hashFile = "$filesDir/$id/sizesHash.txt";
}
else
{
	$hashFile = "$filesDir/sizesHash.txt";
}

if ($submittedFile)
{
	$filesDir = "$filesDir/$$";
	mkdir($filesDir);
	chdir $filesDir;
	my $i = 1;
	while (1)
	{
		my $param             = "fastaFile" . $i;
		my $submittedFileName = param( 'fastaFile' . $i );
		if ($submittedFileName)
		{
			my $fullFileName = &save_file( $submittedFileName, $i );
			push( @fastaFiles, $fullFileName );
		}
		else
		{
			last;
		}
		$i++;
	}
	&readSeqFile;
}
elsif ($id)
{
	$filesDir .= "/$id";
	chdir $filesDir;
	if ($readHash) { &readHashFile; }
	else
	{
		&listFiles;
		&readSeqFile;
	}
}
else
{
	&printForm;
	exit;
}

#
# must have an id, will dump hash to read it later.
#
if ($dumpHash)
{
	&dumpHash;
}
else
{
	&drawFigure;
}

sub dumpHash
{
	open( HASH, ">$hashFile" )
	  || die "ERROR: can't open $hashFile for writing\n";

	foreach my $fasta (@fastaFiles)
	{
		print HASH "\t", $fasta;
	}
	print HASH "\n";

	foreach my $xVal ( sort { $a <=> $b } keys %yVals )
	{
		print HASH $xVal;
		foreach my $fasta (@fastaFiles)
		{
			print HASH "\t";
			if ( $yVals{$xVal}{$fasta} ) { print HASH $yVals{$xVal}{$fasta} }
		}
		print HASH "\n";
	}
	close HASH;
	exit;
}

sub readHashFile
{
	open( HASH, "$hashFile" ) || die "ERROR: can't read $hashFile\n";
	my $firstLine = <HASH>;
	chomp $firstLine;
	( $nothing, @fastaFiles ) = split( "\t", $firstLine );

	foreach my $line (<HASH>)
	{
		chomp $line;
		my ( $x, @values ) = split( "\t", $line );
		push( @xVals, $x );
		my $i = 0;
		while (1)
		{
			if ( $i > $#fastaFiles ) { last; }
			{
				my $y = $values[$i];
				if ($y)
				{
					$yVals{$x}{ $fastaFiles[$i] } = $y;
					if ( $y > $maxY ) { $maxY = $y; }
				}
				else { $yVals{$x}{ $fastaFiles[$i] } = 'undef'; }
				$i++;
			}
		}
		if ( $x < $minSize ) { $minSize = $x; }
		if ( $x > $maxSize ) { $maxSize = $x; }
	}
	close HASH;
}

sub drawFigure
{

	my $count = $maxSize - $minSize;
	if ( $count < 0 ) { exit; }
	my $graph = GD::Graph::lines->new( $count * 10, 600 );

	my @data = ( \@xVals );
	foreach my $dataset (@fastaFiles)
	{
		my @values;
		foreach my $x (@xVals)
		{
			push @values, $yVals{$x}{$dataset};
		}
		push @data, \@values;
	}
	$graph->set_legend(@fastaFiles);

	$graph->set(
		x_label       => 'Read length',
		y_label       => 'Count',
		title         => 'Read length distribution',
		y_max_value   => $maxY,
		y_tick_number => 10,
		x_label_skip  => 5,
		y_label_skip  => 1
	) or die $graph->error;

	my $gd = $graph->plot( \@data ) or die $graph->error;

	my $q = new CGI;
	print $q->header( -type => 'image/png' );
	binmode STDOUT;
	print $gd->png;
}

sub listFiles
{
	opendir( DIR, $filesDir ) || die("Cannot open $filesDir\n");
	my @thefiles = readdir(DIR);
	closedir(DIR);
	foreach my $file (@thefiles)
	{
		if ( $file =~ /(\d+).fasta$/ )
		{
			push( @fastaFiles, $file );
		}
	}
}

sub readSeqFile
{

	#    if($fastaFile){push @fastaFiles, $fastaFile; }
	#    elsif($filesDir)
	my %xVals;
	foreach my $fasta (@fastaFiles)
	{
		open( IN, $fasta ) || die "ERROR: can't open $fasta\n\a";
		my %sizesHash;
		my $size    = 0;
		my $seqName = '';
		foreach my $line (<IN>)
		{
			chomp $line;
			if ( $line =~ />(.+)/ )
			{
				if ($seqName)
				{
					$sizesHash{$size}++;
					if ( $size < $minSize ) { $minSize = $size; }
					if ( $size > $maxSize ) { $maxSize = $size; }
				}
				$seqName = $1;
				$size    = 0;
			}
			else
			{
				$size += length $line;
			}
		}
		close IN;
		$sizesHash{$size}++;

		my $minX;
		my $maxX;
		unless ($minSize2Show) { $minSize2Show = $minSize; }
		if ( $minSize < $minSize2Show ) { $minSize = $minSize2Show; }
		for ( my $i = $minSize; $i <= $maxSize; $i++ )
		{
			if ( exists $sizesHash{$i} )
			{
				$xVals{$i} = 1;
				$yVals{$i}{$fasta} = $sizesHash{$i};

				#	    $hash2print{$i}{$fasta}=$sizesHash{$i};
				if ( $sizesHash{$i} > $maxY ) { $maxY = $sizesHash{$i}; }
			}
			else
			{
				$xVals{$i} = 1;
				$yVals{$i}{$fasta} = undef;

				#	    push(@$yVals{$fasta}, undef);
				#	    $hash2print{$i}{$fasta}='';
			}
		}
	}
	@xVals = keys %xVals;
	@xVals = sort { $a <=> $b } @xVals;
}

# Saves user-submitted file
#
sub save_file
{
	my ( $file, $count ) = @_;
	my $localFileName = $count . ".fasta";
	my $filename      = "$filesDir/$localFileName";

	if ($file)
	{
		open( OUTFILE, ">$filename" ) || print "Can't write to $filename\n";
		my $buffer;
		while ( my $bytesread = read( $file, $buffer, 1024 ) )
		{
			print OUTFILE $buffer;
		}
		close OUTFILE;

		#	system "cp $file $filename";
	}
	return $localFileName;
}

sub printForm
{
	print <<EOF;
Content-Type: text/html;

<HTML>
<HEAD>
<TITLE>Sequence sizes distribtution</TITLE>
</HEAD>
<BODY>
<H2>Sequence sizes distribtution</H2>
<HR><BR>
<FORM enctype="multipart/form-data" method=POST>
<B>Upload your fasta files:</B><BR>
File 1 <INPUT SIZE=20 NAME="fastaFile1" TYPE="file"><BR>
File 2 <INPUT SIZE=20 NAME="fastaFile2" TYPE="file"><BR>
File 3 <INPUT SIZE=20 NAME="fastaFile3" TYPE="file"><BR>
File 4 <INPUT SIZE=20 NAME="fastaFile4" TYPE="file"><BR>
File 5 <INPUT SIZE=20 NAME="fastaFile5" TYPE="file"><BR>
<BR><B>Start graph from sequence size</B>
<INPUT SIZE=3 NAME="minSize" TYPE="TEXT"><BR>
<P>
<INPUT TYPE=SUBMIT>
</FORM>
<BR>
<HR>
<font size=-1><I>By Victor Kunin</font></I>
</BODY>
</HTML>
EOF
}
