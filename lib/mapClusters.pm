use strict;

sub loadDataStructures
{
	my ( $sequenceFile, $mclFile, $clustersFile ) = @_;
	my ( $finalClustersRef, $seqHashRef ) =
	  &loadFinalSequenceFile($sequenceFile);
	my ( $clustersOfClustersHashRef, $parentClustersHashRef ) =
	  &readMclFile( $mclFile, $finalClustersRef );
	my ($subClusters2readsRef) =
	  &readSubClustersFile( $clustersFile, $parentClustersHashRef );

	return (
		$clustersOfClustersHashRef, $parentClustersHashRef,
		$subClusters2readsRef,      $seqHashRef );
}

# Reads the final sequence file in order to find which Subclusters are now reps of the Superclusters
# Receives sequence file as input
# Makes a hash, in which each supercluster rep is a key, with all values set to 1
sub loadFinalSequenceFile
{
	print STDERR "Loading final sequence file...";
	my ($sequenceFile) = @_;
	my %finalClusters;
	my $count   = 0;
	my $seq     = '';
	my $seqName = '';
	my %seqHash;

	open( IN, $sequenceFile ) || die "ERROR: can't open $sequenceFile\n";
	while (my $line = <IN>)
	{
		chomp $seq;
		if ( $line =~ />Cluster(\d+)/ )
		{
			if ($seqName)
			{
				$seqHash{$seqName} = $seq;
			}
			$seqName           = $1;
			$seq               = '';
			$finalClusters{$1} = 0;
			$count++;
		}
		else
		{
			$seq .= $line;
		}
	}

	if ($seqName)
	{
		$seqHash{$seqName} = $seq;
	}
	close IN;
	print STDERR $count, " sequences loaded\n";

	return \%finalClusters, \%seqHash;
}

# Reads MCL file, and assumes that the loadFinalSequenceFile was already called.
# receives the MCL file and the reference to the hash created by loadFinalSequenceFile
# Outputs reference to: i) Hash with each subcluster rep as a key, with all subclusters of the supercluster as secondary keys
#                      ii) Hash in which each subcluster has a reference to its supercluster rep.
sub readMclFile
{
	print STDERR "Loading Superclusters file...";
	my $superclusterCount;
	my $subclusterCount;

	my ( $mclFile, $finalClustersRef ) = @_;
	my %finalClusters = %{$finalClustersRef};

	open( IN, $mclFile ) || die "ERROR: can't open $mclFile\n\a";
	my $count;
	my %clustersOfClustersHash;
	my %parentClustersHash;
	while (my $line = <IN>)
	{
		chomp $line;

		my $parentCluster = '';
		if ( $line =~ /(\S+)/ )
		{
			$count++;
			my @clusters = split /\s+/, $line;
			foreach my $cluster (@clusters)
			{
				my ($clusterNum) = $cluster =~ /Cluster(\d+)/;
				if ( exists $finalClusters{$clusterNum} )
				{
					if ($parentCluster)
					{
						die
						  "ERROR: parent cluster exists already $parentCluster, can't add $clusterNum\n";
					}
					$parentCluster = $clusterNum;
				}
			}
			if ($parentCluster)
			{
				$superclusterCount++;
				foreach my $cluster (@clusters)
				{
					$subclusterCount++;
					my ($clusterNum) = $cluster =~ /Cluster(\d+)/;
					$parentClustersHash{$clusterNum} = $parentCluster;
					$clustersOfClustersHash{$parentCluster}{$clusterNum} = 1;
				}
			}
		}

	}
	close IN;
	print STDERR
	  " $superclusterCount superclusters and $subclusterCount subclusters loaded\n";

	return \%clustersOfClustersHash, \%parentClustersHash;
}

# This subroutine reads the 100% identical clusters to connect clusters to the reads
# receives a reference to clustersOfClusters hash
# outputs a has of subclusters to reads
sub readSubClustersFile
{
	my ( $clustersFile, $parentClusterHashRef ) = @_;

	print STDERR "Reading subclusters file $clustersFile...";
	my $clusterscount = 0;
	my $readsCount    = 0;
	my $allClusters   = 0;

	open( IN, $clustersFile ) || die "ERROR: can't open $clustersFile\n\a";
	my %subClusters2reads;
	while (my $line = <IN>)
	{
		chomp $line;
		if ( $line =~ /Cluster(\d+) (\d+)\t(.+)/ )
		{
			my $cluster_id   = $1;
			my $membersCount = $2;
			my $reads_string = $3;
			$allClusters++;
			my $parentCluster = $$parentClusterHashRef{$cluster_id};
			if ($parentCluster)
			{
				$clusterscount++;

				my @readsList = split( /\s/, $reads_string );
				foreach my $read (@readsList)
				{
					$readsCount++;
					$subClusters2reads{$cluster_id}{$read} = 1;
				}
			}
		}
	}
	close IN;

	print STDERR
	  "$clusterscount subclusters and $readsCount reads read. Total number of subclusters is $allClusters\n";
	return \%subClusters2reads;
}

sub readPrimers
{
	my ($primersFile) = @_;
	open( IN, $primersFile ) || die "ERROR: can't open $primersFile\n\a";
	my %primersHash;
	my %datasetsNamesHash;

	while ( my $line = <IN> )
	{
		chomp $line;
		if ( $line =~ /^(\S+)\s+(\S+)/ )
		{
			my $primer = uc $2;
			$primersHash{$primer}  = $1;
			$datasetsNamesHash{$1} = 1;
		}
	}

	close IN;
	return ( \%primersHash, \%datasetsNamesHash );
}

sub readOriginalFastaAndAssignDatasets
{
	print STDERR "Reading the original fasta file...";
	my $count    = 0;
	my $noPrimer = 0;

	my ( $fastaFile, $primersHashRef, $primer_length ) = @_;

	#    my $forward_primer_length = 25;
	#    my $reverse_primer_length = 20;

	open( IN, $fastaFile ) || die "ERROR: can't open $fastaFile\n";
	my $seqName;
	my $seq;
	my $primer;

	my %datasetsHash;
	while (my $line = <IN>)
	{

		chomp $line;
		if ( $line =~ />(\S+)/ )
		{
			if ( $seqName && $primer )
			{
				$datasetsHash{$seqName} = $primer;
				$count++;
			}
			$seqName = $1;
			$seq     = '';
			$primer  = '';
		}
		else
		{
			$seq .= $line;

			#	    my $primerF = substr ($seq, 0, $forward_primer_length);
			#	    my $primerR = substr ($seq, 0, $reverse_primer_length);
			my $primerRec = substr( $seq, 0, $primer_length );

  #	    if (exists $$primersHashRef{$primerF})	    {		$primer = $primerF;	    }
  #	    elsif (exists $$primersHashRef{$primerR})   {		$primer = $primerR;	    }
			if ( exists $$primersHashRef{$primerRec} ) { $primer = $primerRec; }
			else                                       { $noPrimer++; }
		}
	}

	close IN;

	if ( $seqName && $primer )
	{
		$datasetsHash{$seqName} = $primer;
		$count++;
	}
	else { $noPrimer++; }
	print STDERR "read $count reads\n";
	print STDERR "Sequences with no matches to primers: $noPrimer\n";
	return \%datasetsHash;
}

sub readPrimersAndFasta_AssignDatasets
{
	my ( $primers_fasta_file, $datasetsHashRef ) = @_;
	my %datasetsNamesHash;
	my $datasets2PrimersHashRef;
	open( MAP, $primers_fasta_file )
	  || die "ERROR: can't open $primers_fasta_file\n";

	foreach my $line (<MAP>)
	{
		if ( $line =~ /^(\S+)\s+(\S+)/ )
		{
			( $datasetsHashRef, $datasets2PrimersHashRef ) =
			  &readPrimersAndFasta( $1, $2, $datasetsHashRef );
			print STDERR scalar keys %{$datasetsHashRef};
			print STDERR " reads assigned in total after reading $2\n";
			my @sets = keys %{$datasets2PrimersHashRef};
			while ( my $set = shift @sets )
			{
				$datasetsNamesHash{$set} = 1;
			}
			print STDERR " done while loop\n";
		}
	}
	close MAP;
	return ( $datasetsHashRef, \%datasetsNamesHash );
}

sub readPrimersAndFasta
{
	my ( $primersFile, $fastaFile, $datasetsHashRef ) = @_;
	my %datasetsHash = %{$datasetsHashRef};
	my ( $primersHashRef, $datasets2PrimersHash ) = &readPrimers($primersFile);

	my $primer_length;
	{
		my @primers = keys %{$primersHashRef};
		$primer_length = length $primers[0];
	}

	my $count    = 0;
	my $noPrimer = 0;

	print STDERR "Reading $fastaFile\n";
	open( IN, $fastaFile ) || die "ERROR: can't open $fastaFile\n";
	my $seqName;
	my $seq;
	my $primer;

	my $printLine = 0;
	while (my $line = <IN>)
	{
		chomp $line;
		if ( $line =~ />(\S+)/ )
		{
			if ( $seqName && $primer )
			{
				$datasetsHash{$seqName} = $$primersHashRef{$primer};
				$count++;
			}
			$seqName = $1;
			$seq     = '';
			$primer  = '';
		}
		else
		{
			$seq .= $line;
			my $primerRec = substr( $seq, 0, $primer_length );

			if ( exists $$primersHashRef{$primerRec} ) { $primer = $primerRec; }
			else                                       { $noPrimer++; }
		}
	}

	close IN;
	print STDERR "read $count reads\n";

	if ( $seqName && $primer )
	{
		$datasetsHash{$seqName} = $$primersHashRef{$primer};
		$count++;
	}
	print STDERR "Sequences with no matches to primers: $noPrimer\n";

	return ( \%datasetsHash, \%$datasets2PrimersHash );
}

1;

