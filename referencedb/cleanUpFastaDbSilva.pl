#!/usr/bin/env perl

use strict;
use warnings;

die("Infile, Outfile required\n") unless @ARGV == 2;
my ($inFile, $outFile) = @ARGV;
open(my $in, '<', $inFile) or die($!);
open(my $out, '>', $outFile) or die($!);
my $flag = 0; # print if true
while (my $line = <$in>)
{
    if ($line =~ /^>(\S+) (.+)$/)
    {
        my $id = $1;
        my @tax = split(/;\s?/, $2);
        if ( $flag = @tax >= 4 ? 1:0 )
        {
            print $out '>', join('; ', $id, @tax);
        } else
        {
            print $line;
        }
    } elsif ( $flag )
    {
        $line =~ s/U/T/gi; 
        print $out $line;
    }
}
