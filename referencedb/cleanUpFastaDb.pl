#!/usr/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

$|=1;

my ($inFile, $outFile) = @ARGV;

unless ($inFile && $outFile)
{
    die "
USAGE:
$0 inFile outFile
";
}


my $in = Bio::SeqIO->new(-file => $inFile, -format=>'Fasta');
my $out = Bio::SeqIO->new(-file => ">$outFile", -format=>'Fasta');

while (my $seq = $in->next_seq() )
{
    my $desc= $seq->desc;
    my @fields = split ";\s?", $desc;
    if(scalar @fields < 4)
    {
	print $seq->id, ' ' , $desc, "\n";
    }
    else
    {
	$out->write_seq($seq);
    }
}
